# Sparschwein

![alt Logo](frontend/public/logo.svg)

_Sparschwein_ translates to **Piggy Bank**. It's a simple web application to manage pocket money.

_Sparschwein_ should eventually have the following features:

## User Features

- each user can have one account
- users can check their account balance
- users can withdraw money from their account
- users can see the transaction log of their account
- users can see scheduled recurring payments to their account

## Admin User Features

- admin users can withdraw money from user accounts
- admin users can add money to user accounts
- admin users can configure scheduled recurring payments to user accounts
- admin users can check all transaction logs
- admin users can add or delete users

## User Authentication

To be decided and implemented.

## Database

This project is set up to use PostgreSQL, porting it to another
database will probably require changes to the migrations.

Add a file named `.env` to the directory from where you are
running the binary, and add something along the following line
to it in order to configure the database:

```
DATABASE_URL=postgres://<user>:<password>/<database>
```

## Tests

For information on building and running the backend acceptance
tests, please refer to the [README](tests/README.md) in the
tests folder.

## License

Sparschwein is licensed under the MIT license.
