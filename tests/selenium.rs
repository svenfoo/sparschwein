use crate::common::dependencies::{drop_database, start_backend, start_chrome_driver};
use crate::crud::all::CRUD;
use async_trait::async_trait;
use cucumber::{World, WorldInit};
use dotenv::dotenv;
use env_logger::Env;
use image::io::Reader;
use log::warn;
use openapi::models::{ScheduleResponse, TransactionResponse, Translation, UserResponse};
use pixelmatch::pixelmatch;
use pixelmatch::Options;
use std::convert::Infallible;
use std::fs::File;
use std::io::{BufReader, Cursor};
use std::path::Path;
use std::time::Duration;
use std::{env, fs};
use thirtyfour::prelude::*;
use thirtyfour::OptionRect;
use tokio::sync::oneshot;

mod common;
mod crud;
mod fakers;
mod gui_steps;

#[derive(Debug)]
pub enum Language {
    En,
    De,
}

impl Language {
    pub fn to_string(&self) -> String {
        match self {
            Language::En => "en",
            Language::De => "de",
        }
        .to_string()
    }
}

#[derive(Debug, WorldInit)]
pub struct PiggiesWorld {
    driver: WebDriver,
    translation: Translation,
    schedules: CRUD<ScheduleResponse>,
    transactions: CRUD<TransactionResponse>,
    users: CRUD<UserResponse>,
    passwords: CRUD<String>,
    language: Language,
}

impl PiggiesWorld {
    async fn take_screenshot(&self, name: &str) {
        let recording = env::var("RECORD").map(|_| true).unwrap_or_else(|_| false);
        let lang = self.language.to_string();
        let path = format!("tests/screenshots/{name}-{lang}.png");

        let current = self
            .driver
            .screenshot_as_png()
            .await
            .expect("Can not take screenshot");
        let mut cropped = Reader::new(Cursor::new(current))
            .with_guessed_format()
            .unwrap()
            .decode()
            .unwrap();
        cropped = cropped.crop(0, 0, 1024, 550);
        let mut after = Vec::new();
        // this is necessary because otherwise Decoder inside of pixelmatch cannot decode it
        cropped
            .write_to(&mut Cursor::new(&mut after), image::ImageOutputFormat::Png)
            .unwrap();
        if recording {
            fs::write(path, after).unwrap();
        } else {
            let before = fs::read(&path).unwrap();
            let mut diff = Vec::new();
            let num_diff_pixels = pixelmatch(
                before.as_slice(),
                after.as_slice(),
                Some(&mut diff),
                None,
                None,
                Some(Options {
                    threshold: 0.2,
                    ..Default::default()
                }),
            )
            .expect("Can not detect pixel diff");

            let diff_path = format!("tests/screenshots/{name}_diff.png");
            if num_diff_pixels > 0 {
                fs::write(diff_path, diff).unwrap();
            }
        }
    }
}

#[async_trait(?Send)]
impl World for PiggiesWorld {
    type Error = Infallible;

    async fn new() -> Result<Self, Infallible> {
        let language = match env::var("LANG") {
            Ok(some) => match some.as_str() {
                "de" => Language::De,
                "en" => Language::En,
                _ => unreachable!(),
            },
            Err(_) => Language::En,
        };
        let lang = language.to_string();
        let mut caps = DesiredCapabilities::chrome();
        let chrome_cap = format!("--accept-lang={lang}");
        caps.add_chrome_arg(&chrome_cap)
            .expect("Can not set language");
        caps.add_chrome_arg("--incognito")
            .expect("Can not set private browsing");
        let driver = WebDriver::new("http://localhost:4444", &caps)
            .await
            .unwrap();
        let delay = Duration::from_millis(100);
        driver.set_implicit_wait_timeout(delay).await.unwrap();
        let r = OptionRect::new().with_size(1024, 768);
        driver.set_window_rect(r).await.unwrap();

        let path = format!("frontend/translations/translations.{lang}.json");
        let file = File::open(path).unwrap();
        let reader = BufReader::new(file);
        let translation: Translation = serde_json::from_reader(reader).unwrap();
        Ok(Self {
            driver,
            translation,
            schedules: Default::default(),
            transactions: Default::default(),
            users: Default::default(),
            passwords: Default::default(),
            language,
        })
    }
}

#[tokio::main]
async fn main() {
    dotenv().ok();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    match Path::new("./chromedriver").is_file() {
        true => {}
        false => {
            warn!("Skipping GUI tests because chromedriver is not present");
            return;
        }
    };

    drop_database().await;

    let (kill_sender_be, kill_receiver_be) = oneshot::channel::<()>();
    let (exit_status_sender_be, exit_status_receiver_be) = oneshot::channel::<()>();
    start_backend(exit_status_sender_be, kill_receiver_be).await;
    let (kill_sender_cd, kill_receiver_cd) = oneshot::channel::<()>();
    let (exit_status_sender_cd, exit_status_receiver_cd) = oneshot::channel::<()>();
    start_chrome_driver(exit_status_sender_cd, kill_receiver_cd).await;

    PiggiesWorld::cucumber()
        .max_concurrent_scenarios(1)
        .after(|_feature, _rule, _scenario, world| {
            Box::pin(async move {
                if let Some(world) = world {
                    world.driver.close().await.unwrap();
                }
            })
        })
        .run_and_exit("./tests/features")
        .await;

    kill_sender_be.send(()).unwrap();
    kill_sender_cd.send(()).unwrap();
    exit_status_receiver_be.await.unwrap();
    exit_status_receiver_cd.await.unwrap();
}
