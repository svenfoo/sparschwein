use crate::crud::all::CRUD;
use async_trait::async_trait;
use common::dependencies::start_backend;
use cucumber::{writer, World, WorldInit, WriterExt};
use dotenv::dotenv;
use openapi::apis::configuration::Configuration;
use openapi::models::{ScheduleResponse, TransactionResponse, UserResponse};
use std::convert::Infallible;
use std::fs;
use tokio::sync::oneshot;

mod common;
mod crud;
mod fakers;
mod steps;

#[derive(Debug, WorldInit)]
pub struct PiggiesWorld {
    configuration: Configuration,
    schedules: CRUD<ScheduleResponse>,
    transactions: CRUD<TransactionResponse>,
    users: CRUD<UserResponse>,
    passwords: CRUD<String>,
}

#[async_trait(?Send)]
impl World for PiggiesWorld {
    type Error = Infallible;

    async fn new() -> Result<Self, Infallible> {
        Ok(Self {
            configuration: Configuration::default(),
            schedules: CRUD::default(),
            transactions: CRUD::default(),
            users: CRUD::default(),
            passwords: CRUD::default(),
        })
    }
}

#[tokio::main]
async fn main() {
    dotenv().ok();

    let (kill_sender_be, kill_receiver_be) = oneshot::channel::<()>();
    let (exit_status_sender_be, exit_status_receiver_be) = oneshot::channel::<()>();
    start_backend(exit_status_sender_be, kill_receiver_be).await;

    let file = fs::File::create(dbg!(format!(
        "{}/target/junit.xml",
        env!("CARGO_MANIFEST_DIR")
    )))
    .expect("Can not create file");

    PiggiesWorld::cucumber()
        .with_writer(
            writer::Basic::stdout()
                .summarized()
                .tee::<PiggiesWorld, _>(writer::JUnit::for_tee(file, 1))
                .normalized(),
        )
        .run_and_exit("tests/features")
        .await;

    kill_sender_be.send(()).unwrap();
    exit_status_receiver_be.await.unwrap();
}
