use fake::faker::internet::en::FreeEmail;
use fake::faker::internet::en::Password;
use fake::faker::lorem::en::Sentence;
use fake::faker::name::en::FirstName;
use fake::uuid::UUIDv4;
use fake::Fake;
use fake::Faker;
use openapi::models::{
    CreateSchedule, CreateTransaction, CreateUser, Frequency, Role, UpdateSchedule, UpdateUser,
    Withdraw,
};
use rand::prelude::SliceRandom;
use std::ops::Range;

fn random_role() -> Role {
    let mut rng = rand::thread_rng();
    let options = [Role::User, Role::Admin];
    *options.choose(&mut rng).unwrap()
}

fn random_frequency() -> Frequency {
    let mut rng = rand::thread_rng();
    let options = [Frequency::Monthly, Frequency::Weekly];
    *options.choose(&mut rng).unwrap()
}

fn random_positive_amount() -> f64 {
    let cents = (1..).fake::<i32>();
    cents as f64 * 100.0
}

pub fn random_negative_amount() -> f64 {
    let cents = (..-1).fake::<i32>();
    cents as f64 * 100.0
}

fn random_amount() -> f64 {
    let cents = Faker.fake::<i32>();
    cents as f64 * 100.0
}

fn random_comment() -> String {
    Sentence(0..5).fake()
}

pub fn fake_create_user() -> CreateUser {
    CreateUser {
        name: FirstName().fake(),
        role: random_role(),
        email: FreeEmail().fake(),
        password: Password(Range { start: 6, end: 8 }).fake(),
    }
}

pub fn fake_update_user() -> UpdateUser {
    UpdateUser {
        name: FirstName().fake(),
        role: random_role(),
        email: FreeEmail().fake(),
    }
}

pub fn fake_create_schedule() -> CreateSchedule {
    CreateSchedule {
        user_id: UUIDv4.fake(),
        frequency: random_frequency(),
        amount: random_positive_amount(),
    }
}

pub fn fake_update_schedule() -> UpdateSchedule {
    UpdateSchedule {
        frequency: random_frequency(),
        amount: random_positive_amount(),
    }
}

pub fn fake_create_transaction() -> CreateTransaction {
    CreateTransaction {
        user_id: UUIDv4.fake(),
        amount: random_amount(),
        comment: random_comment(),
    }
}

pub fn fake_withdraw() -> Withdraw {
    Withdraw {
        amount: random_negative_amount(),
        comment: random_comment(),
    }
}
