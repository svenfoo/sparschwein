use crate::gui_steps::common::{click_button, fill_field, navigate};
use crate::gui_steps::users::{check_user, create_new_user};
use crate::PiggiesWorld;
use cucumber::{given, when};
use openapi::models::Role;
use std::env;
use thirtyfour::{Keys, TypingData};

#[given("admin is authenticated")]
async fn authenticate_admin(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    navigate(driver, "sign-in").await;
    world.take_screenshot("sign-in").await;
    let username = env::var("ADMIN_EMAIL").expect("ADMIN_EMAIL must be set");
    let password = env::var("ADMIN_PASSWORD").expect("ADMIN_PASSWORD must be set");
    fill_field(driver, "//input[@type='email']", &username).await;
    let input = TypingData::from(&password) + Keys::Enter;
    fill_field(driver, "//input[@type='password']", input).await;
}

#[when("user is authenticated")]
async fn authenticate_user(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let sign_out = format!("//div[text()='{}']", world.translation.buttons.sign_out);
    click_button(driver, &sign_out).await;
    let sign_in = format!("//div[text()='{}']", world.translation.buttons.sign_in);
    click_button(driver, &sign_in).await;
    let user = world.users.created();
    let password = world.passwords.created();
    fill_field(driver, "//input[@type='email']", &user.email).await;
    let input = TypingData::from(password) + Keys::Enter;
    fill_field(driver, "//input[@type='password']", input).await;
}

#[given("an authenticated user")]
async fn authenticated_user(world: &mut PiggiesWorld) {
    authenticate_admin(world).await;
    create_new_user(world, Some(Role::User)).await;
    check_user(world, &world.users.created()).await;
    authenticate_user(world).await;
}
