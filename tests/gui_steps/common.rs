use cucumber::codegen::Regex;
use steel_cent::{currency::EUR, formatting::france_style, Money};
use thirtyfour::prelude::*;

pub async fn navigate(driver: &WebDriver, url: &str) {
    driver
        .get("http://localhost:3000/".to_owned() + url)
        .await
        .unwrap();
}

pub async fn click_button(driver: &WebDriver, path: &str) {
    let elem = driver
        .query(By::XPath(path))
        .first()
        .await
        .expect(&format!("No button: {path}"));
    elem.wait_until()
        .displayed()
        .await
        .expect(&format!("Timeout waiting for button: {path}"));
    elem.click()
        .await
        .expect(&format!("Can not click button: {path}"));
}

pub async fn fill_field<S>(driver: &WebDriver, path: &str, text: S)
where
    S: Into<TypingData>,
{
    let elem = driver
        .query(By::XPath(path))
        .first()
        .await
        .expect(&format!("No field: {path}"));
    elem.wait_until()
        .displayed()
        .await
        .expect(&format!("Timeout waiting for field: {path}"));
    elem.clear()
        .await
        .expect(&format!("Can not clear field: {path}"));
    elem.send_keys(text)
        .await
        .expect(&format!("Can not fill field: {path}"));
}

pub async fn fill_field_at_index<S>(driver: &WebDriver, path: &str, index: usize, text: S)
where
    S: Into<TypingData>,
{
    let elems = driver
        .query(By::XPath(path))
        .all()
        .await
        .expect(&format!("No field at index {index}: {path}"));
    elems[index].wait_until().displayed().await.expect(&format!(
        "Timeout waiting for field at index {index}: {path}"
    ));
    elems[index]
        .clear()
        .await
        .expect(&format!("Can not clear field at index {index}: {path}"));
    elems[index]
        .send_keys(text)
        .await
        .expect(&format!("Can not fill field at index {index}: {path}"));
}

pub async fn fill_fields(driver: &WebDriver, path: &str, texts: Vec<&String>) {
    let elems = driver.query(By::XPath(path)).all().await.unwrap();
    for (index, elem) in elems.iter().enumerate() {
        elem.wait_until().displayed().await.expect(&format!(
            "Timeout waiting for field at index {index}: {path}"
        ));
        elem.clear()
            .await
            .expect(&format!("Can not clear field at index {index}: {path}"));
        elem.send_keys(texts[index])
            .await
            .expect(&format!("Can not fill field at index {index}: {path}"));
    }
}

pub async fn find_text(driver: &WebDriver, text: &str) {
    let xpath = format!("//*[contains(text(),'{}')]", text);
    let elem = driver
        .query(By::XPath(&xpath))
        .first()
        .await
        .expect(&format!("No text: {text}"));
    elem.wait_until()
        .error("")
        .displayed()
        .await
        .expect(&format!("Timeout waiting for text: {text}"));
}

pub async fn no_text(driver: &WebDriver, text: &str) {
    let xpath = format!("//*[contains(text(),'{}')]", text);
    match driver.query(By::XPath(&xpath)).first().await {
        Ok(_) => assert!(true, "Found element that should not exist: {text}"),
        Err(_) => {}
    };
}

pub async fn is_user_page(driver: &WebDriver) -> bool {
    let current_url = driver.current_url().await.unwrap();
    // courtesy of https://ihateregex.io/expr/uuid/
    let re = Regex::new(
        r"[\da-fA-F]{8}\b-[\da-fA-F]{4}\b-[\da-fA-F]{4}\b-[\da-fA-F]{4}\b-[\da-fA-F]{12}",
    )
    .unwrap();
    re.is_match(&current_url)
}

pub fn amount(amount: f64) -> String {
    let i = amount.floor();
    let f = amount - i;
    let money = Money::of_major_minor(EUR, i as i64, f as i64);
    let mut amount = france_style().display_for(&money).to_string();
    amount = amount
        .replace("\u{a0}€", "€")
        .replace("\u{a0}", "\u{202F}")
        .replace("-", "−");
    amount
}
