use crate::crud::all::ExpectedResponse;
use crate::fakers::all::{fake_create_schedule, fake_update_schedule};
use crate::gui_steps::common::{
    amount, click_button, fill_field, fill_field_at_index, find_text, is_user_page, no_text,
};
use crate::PiggiesWorld;
use cucumber::{given, then, when};
use openapi::models::{Frequency, ScheduleResponse};

#[when("admin creates a new schedule")]
async fn schedule_is_created(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let schedules = format!("//div[text()='{}']", world.translation.titles.schedules);
    click_button(driver, &schedules).await;
    world.take_screenshot("schedules").await;
    create_new_schedule(world).await;
}

#[given("a schedule exists")]
async fn schedule_exists(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let schedules = format!("//div[text()='{}']", world.translation.titles.schedules);
    click_button(driver, &schedules).await;
    create_new_schedule(world).await;
    check_schedule(world, &world.schedules.created()).await;
}

#[when("admin views all schedules")]
async fn all_schedules(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let schedules = format!("//div[text()='{}']", world.translation.titles.schedules);
    click_button(driver, &schedules).await;
}

#[when("user views her schedules")]
async fn user_schedules(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let schedules = format!("//div[text()='{}']", world.translation.titles.schedules);
    click_button(driver, &schedules).await;
}

#[when("admin updates the schedule")]
async fn update_schedule(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let edit = format!("//div[text()='{}']", world.translation.buttons.edit);
    click_button(driver, &edit).await;

    let user = world.users.created();
    let request = fake_update_schedule();

    fill_field(driver, "//input[@type='text']", request.amount.to_string()).await;
    let frequency = frequency_localized(world, request.frequency);
    click_button(driver, &format!("//div[text()='{}']", frequency)).await;
    let submit = format!("//div[text()='{}']", world.translation.buttons.submit);
    click_button(driver, &submit).await;

    let response = world
        .schedules
        .create_expected_response("id".to_string(), &user, &request);
    world.schedules.set_updated(response);
}

#[when("admin deletes the schedule")]
async fn delete_schedule(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let delete = format!("//div[text()='{}']", world.translation.buttons.delete);
    click_button(driver, &delete).await;
    let confirm = format!("//div[text()='{}']", world.translation.buttons.confirm);
    click_button(driver, &confirm).await;
}

#[then("the schedule is present")]
async fn schedule_is_present(world: &mut PiggiesWorld) {
    let created = world.schedules.created();
    check_schedule(world, created).await;
}

#[then("the schedule is updated")]
async fn schedule_is_updated(world: &mut PiggiesWorld) {
    let updated = world.schedules.updated();
    check_schedule(world, updated).await;
}

#[then("the schedule is not present")]
async fn schedule_is_not_present(world: &mut PiggiesWorld) {
    let created = world.schedules.created();
    no_text(&world.driver, &created.name).await;
}

async fn create_new_schedule(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let new_schedule = format!("//div[text()='{}']", world.translation.buttons.new_schedule);
    click_button(driver, &new_schedule).await;
    world.take_screenshot("new-schedule").await;

    let user = world.users.created();
    let request = fake_create_schedule();

    click_button(driver, "//input[@type='text']").await;
    click_button(driver, &format!("//div[text()='{}']", &user.name)).await;
    fill_field_at_index(
        driver,
        "//input[@type='text']",
        1,
        &request.amount.to_string(),
    )
    .await;
    let frequency = frequency_localized(world, request.frequency);
    click_button(driver, &format!("//div[text()='{}']", frequency)).await;
    let submit = format!("//div[text()='{}']", world.translation.buttons.submit);
    click_button(driver, &submit).await;

    let response = world
        .schedules
        .create_expected_response("id".to_string(), user, &request);
    world.schedules.set_created(response)
}

async fn check_schedule(world: &PiggiesWorld, schedule: &ScheduleResponse) {
    let driver = &world.driver;
    if !is_user_page(driver).await {
        find_text(driver, &schedule.name).await;
    }
    let frequency = frequency_localized(world, schedule.frequency);
    find_text(driver, &frequency).await;
    let amount = amount(schedule.amount);
    find_text(driver, &amount).await;
}

fn frequency_localized(world: &PiggiesWorld, frequency: Frequency) -> &String {
    match frequency {
        Frequency::Monthly => &world.translation.forms.frequency_monthly,
        Frequency::Weekly => &world.translation.forms.frequency_weekly,
    }
}
