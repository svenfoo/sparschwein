use crate::crud::all::ExpectedResponse;
use crate::fakers::all::fake_create_transaction;
use crate::gui_steps::common::{amount, click_button, fill_field_at_index, find_text, navigate};
use crate::PiggiesWorld;
use cucumber::{then, when};
use openapi::models::TransactionResponse;

#[when("admin creates a new transaction")]
async fn transaction_exists(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let transactions = format!("//div[text()='{}']", world.translation.titles.transactions);
    click_button(driver, &transactions).await;
    world.take_screenshot("transactions").await;

    let new_transaction = format!(
        "//div[text()='{}']",
        world.translation.buttons.new_transaction
    );
    click_button(driver, &new_transaction).await;
    world.take_screenshot("new-transaction").await;

    let user = world.users.created();
    let request = fake_create_transaction();

    click_button(driver, "//input[@type='text']").await;
    click_button(driver, &format!("//div[text()='{}']", &user.name)).await;
    fill_field_at_index(
        driver,
        "//input[@type='text']",
        1,
        &request.amount.to_string(),
    )
    .await;
    fill_field_at_index(
        driver,
        "//input[@type='text']",
        2,
        &request.comment.to_string(),
    )
    .await;
    let submit = format!("//div[text()='{}']", world.translation.buttons.submit);
    click_button(driver, &submit).await;

    let response = world
        .transactions
        .create_expected_response("id".to_string(), &user, &request);
    world.transactions.set_created(response);
    let mut updated = user.clone();
    updated.balance += request.amount;
    world.users.set_updated(updated);
}

#[when("admin views all transactions")]
async fn all_transactions(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    navigate(driver, "transactions").await;
}

#[when("user views her transactions")]
async fn user_transactions(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let transactions = format!("//div[text()='{}']", world.translation.titles.transactions);
    click_button(driver, &transactions).await;
}

#[then("the transaction is present")]
async fn transaction_is_present(world: &mut PiggiesWorld) {
    let created = world.transactions.created();
    check_transaction(world, created).await;
}

async fn check_transaction(world: &PiggiesWorld, transaction: &TransactionResponse) {
    let driver = &world.driver;
    find_text(driver, &transaction.creator_name.as_ref().unwrap()).await;
    find_text(driver, &transaction.created_at).await;
    let amount = amount(transaction.amount);
    find_text(driver, &amount).await;
    find_text(driver, &transaction.comment).await;
}
