use crate::crud::all::{admin_user, ExpectedResponse};
use crate::fakers::all::{fake_create_user, fake_update_user, fake_withdraw};
use crate::gui_steps::common::{
    click_button, fill_field, fill_fields, find_text, is_user_page, no_text,
};
use crate::PiggiesWorld;
use cucumber::{given, then, when};
use openapi::models::{Role, UserResponse};
use thirtyfour::{Keys, TypingData};

#[when("admin creates a new user")]
async fn user_is_created(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let users = format!("//div[text()='{}']", world.translation.titles.users);
    click_button(driver, &users).await;
    world.take_screenshot("users").await;
    create_new_user(world, None).await;
}

#[given("a user exists")]
async fn user_exists(world: &mut PiggiesWorld) {
    create_new_user(world, Some(Role::User)).await;
}

#[when("admin views all users")]
async fn all_users(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let users = format!("//div[text()='{}']", world.translation.titles.users);
    click_button(driver, &users).await;
}

#[when(regex = r"^(admin|user) views the user$")]
async fn view_user(world: &mut PiggiesWorld, role: String) {
    let driver = &world.driver;
    match role.as_str() {
        "admin" => {
            let view = format!("//div[text()='{}']", world.translation.buttons.view);
            click_button(driver, &view).await;
        }
        "user" => {
            let home = format!("//div[text()='{}']", world.translation.titles.home);
            click_button(driver, &home).await;
        }
        _ => unreachable!(),
    }
}

#[when("admin updates the user")]
async fn update_user(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let edit = format!("//div[text()='{}']", world.translation.buttons.edit);
    click_button(driver, &edit).await;

    let created = world.users.created();
    let id = created.id.clone();
    let mut request = fake_update_user();
    // A hack so that the user appears first
    request.name = format!("0{}", &request.name);
    fill_field(driver, "//input[@type='text']", &request.name).await;
    fill_field(driver, "//input[@type='email']", &request.email).await;
    let role = role_localized(world, request.role);
    click_button(driver, &format!("//div[text()='{}']", role)).await;
    let submit = format!("//div[text()='{}']", world.translation.buttons.submit);
    click_button(driver, &submit).await;

    let response = world
        .users
        .create_expected_response(id, &admin_user(), &request);
    world.users.set_updated(response);
}

#[when("admin deletes the user")]
async fn delete_user(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let delete = format!("//div[text()='{}']", world.translation.buttons.delete);
    click_button(driver, &delete).await;
    let confirm = format!("//div[text()='{}']", world.translation.buttons.confirm);
    click_button(driver, &confirm).await;
}

#[when("user withdraws money")]
async fn create_user_transaction(world: &mut PiggiesWorld) {
    let driver = &world.driver;
    let withdraw = format!("//div[text()='{}']", world.translation.buttons.withdraw);
    click_button(driver, &withdraw).await;
    world.take_screenshot("withdraw").await;

    let user = world.users.created();
    let mut request = fake_withdraw();
    request.amount = -request.amount;
    let amount = request.amount.to_string();
    let texts = vec![&amount, &request.comment];
    fill_fields(driver, "//input[@type='text']", texts).await;
    let submit = format!("//div[text()='{}']", world.translation.buttons.submit);
    click_button(driver, &submit).await;

    let response = world
        .transactions
        .create_expected_response("id".to_string(), &user, &request);
    world.transactions.set_created(response);
    let mut updated = user.clone();
    updated.balance += request.amount;
    world.users.set_updated(updated);
}

#[then("the user is present")]
async fn user_is_present(world: &mut PiggiesWorld) {
    let created = world.users.created();
    check_user(world, created).await;
}

#[then("the user is updated")]
async fn user_is_updated(world: &mut PiggiesWorld) {
    let updated = world.users.updated();
    check_user(world, updated).await;
}

#[then("the user is not present")]
async fn user_is_not_present(world: &mut PiggiesWorld) {
    let created = world.users.created();
    no_text(&world.driver, &created.name).await;
}

pub async fn create_new_user(world: &mut PiggiesWorld, role: Option<Role>) {
    let driver = &world.driver;
    let users = format!("//div[text()='{}']", world.translation.titles.users);
    click_button(driver, &users).await;
    let new_user = format!("//div[text()='{}']", world.translation.buttons.new_user);
    click_button(driver, &new_user).await;
    world.take_screenshot("new-user").await;

    let mut request = fake_create_user();
    if let Some(assigned_role) = role {
        request.role = assigned_role;
    }
    // A hack so that the user appears first
    request.name = format!("0{}", &request.name);
    let texts = vec![&request.name, &request.password];
    fill_fields(driver, "//input[@type='text']", texts).await;
    let role = role_localized(world, request.role);
    click_button(driver, &format!("//div[text()='{}']", role)).await;
    let input = TypingData::from(&request.email) + Keys::Enter;
    fill_field(driver, "//input[@type='email']", input).await;

    let response = world
        .users
        .create_expected_response("id".to_string(), &admin_user(), &request);
    world.users.set_created(response);
    world.passwords.set_created(request.password.clone());
}

pub async fn check_user(world: &PiggiesWorld, user: &UserResponse) {
    let driver = &world.driver;
    if !is_user_page(driver).await {
        find_text(driver, &user.name).await;
        if user.role == Role::Admin {
            let admin = &world.translation.forms.role_admin;
            find_text(driver, admin).await;
        }
    }
}

fn role_localized(world: &PiggiesWorld, role: Role) -> &String {
    match role {
        Role::User => &world.translation.forms.role_user,
        Role::Admin => &world.translation.forms.role_admin,
    }
}
