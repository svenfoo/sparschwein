# Cucumber

The acceptance tests for the backend are written in Cucumber.
They use a Rust client that is generated from the OpenAPI spec.
A copy of this spec is found in the file `openapi.json` in the
toplevel directory.

The frontend code also has tests that use an elm client generated
from the same OpenAPI spec.

## Updating the OpenAPI spec

After making changes to the API, this file needs to be updated.
You can do so by starting the sparschwein executable and
downloading the spec:

```
cargo run &
curl -o openapi.json http://localhost:3000/api/v1/openapi
```

## Generating the OpenAPI clients

You will need _OpenAPI Generator_ to generate the client, see
[here](https://openapi-generator.tech/docs/installation/) for
installation instructions.

To regenerate the clients, run

```
openapi-generator batch openapi-cli/rust.yaml
openapi-generator batch openapi-cli/elm.yaml
```

## Running the tests

```
cargo test --test spreewald
```

With tags:

```
cargo test --test selenium -- -t @foo
```
s