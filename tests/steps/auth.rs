use crate::PiggiesWorld;
use cucumber::{given, when};
use openapi::apis::default_api::create_auth;
use openapi::models::{CreateToken, Role};
use std::env;

use super::users::create_new_user;

#[given("admin is authenticated")]
async fn authenticate_admin(world: &mut PiggiesWorld) {
    let request = CreateToken {
        email: env::var("ADMIN_EMAIL").expect("ADMIN_EMAIL must be set"),
        password: env::var("ADMIN_PASSWORD").expect("ADMIN_PASSWORD must be set"),
    };
    let response = create_auth(&world.configuration, request).await.unwrap();
    world.configuration.bearer_access_token = Some(response.token);
}

#[when("user is authenticated")]
async fn authenticate_user(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let password = world.passwords.created();
    let request = CreateToken {
        email: user.email.clone(),
        password: password.clone(),
    };
    let response = create_auth(&world.configuration, request).await.unwrap();
    world.configuration.bearer_access_token = Some(response.token);
}

#[given("an authenticated user")]
async fn authenticated_user(world: &mut PiggiesWorld) {
    authenticate_admin(world).await;
    create_new_user(world, Some(Role::User)).await;
    authenticate_user(world).await;
}
