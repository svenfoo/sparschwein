use crate::crud::all::{admin_user, ExpectedResponse};
use crate::fakers::all::{fake_create_user, fake_update_user, fake_withdraw};
use crate::PiggiesWorld;
use cucumber::{given, then, when};
use openapi::apis::default_api::{
    create_user_transactions, create_users, delete_users, read_all_users, read_users, update_users,
};
use openapi::models::{Role, UserResponse};

#[when("admin creates a new user")]
async fn user_is_created(world: &mut PiggiesWorld) {
    create_new_user(world, None).await;
}

#[given("a user exists")]
async fn user_exists(world: &mut PiggiesWorld) {
    create_new_user(world, Some(Role::User)).await;
}

#[when("admin views all users")]
async fn all_users(world: &mut PiggiesWorld) {
    let response = read_all_users(&world.configuration).await.unwrap();
    world.users.set_all(response);
}

#[when("admin updates the user")]
async fn update_user(world: &mut PiggiesWorld) {
    let created = world.users.created();
    let id = created.id.clone();
    let request = fake_update_user();
    update_users(&world.configuration, &id, request.clone())
        .await
        .unwrap();
    let response = world
        .users
        .create_expected_response(id, &admin_user(), &request);
    world.users.set_updated(response);
}

#[when(regex = r"^(admin|user) views the user$")]
async fn read_user(world: &mut PiggiesWorld) {
    let created = world.users.created();
    let response = read_users(&world.configuration, &created.id.clone())
        .await
        .unwrap();
    world.users.set_all(vec![response]);
}

#[when("admin deletes the user")]
async fn delete_user(world: &mut PiggiesWorld) {
    let created = world.users.created();
    delete_users(&world.configuration, &created.id.clone())
        .await
        .unwrap();
}

#[when("user withdraws money")]
async fn create_user_transaction(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let request = fake_withdraw();
    let amount = request.amount;
    let id = create_user_transactions(&world.configuration, &user.id.clone(), request.clone())
        .await
        .unwrap();
    let response = world
        .transactions
        .create_expected_response(id, &user, &request);
    world.transactions.set_created(response);
    let mut updated = user.clone();
    updated.balance += amount;
    world.users.set_updated(updated);
}

#[then("the user is present")]
async fn user_is_present(world: &mut PiggiesWorld) {
    let created = world.users.created();
    check_user(world, created).await;
}

#[then("the user is updated")]
async fn user_is_updated(world: &mut PiggiesWorld) {
    let updated = world.users.updated();
    check_user(world, updated).await;
}

#[then("the user is not present")]
async fn user_is_not_present(world: &mut PiggiesWorld) {
    let created = world.users.created();
    assert_eq!(world.users.all().iter().find(|v| v.id == created.id), None);
}

pub async fn create_new_user(world: &mut PiggiesWorld, role: Option<Role>) {
    let mut request = fake_create_user();
    if let Some(assigned_role) = role {
        request.role = assigned_role;
    }
    let password = request.password.clone();
    let id = create_users(&world.configuration, request.clone())
        .await
        .unwrap();
    let response = world
        .users
        .create_expected_response(id, &admin_user(), &request);
    world.users.set_created(response);
    world.passwords.set_created(password);
}

async fn check_user(world: &PiggiesWorld, user: &UserResponse) {
    let existing = world.users.all().iter().find(|v| v.id == user.id).unwrap();
    assert_eq!(user.name, existing.name);
    assert_eq!(user.balance, existing.balance);
    assert_eq!(user.email, existing.email);
    assert_eq!(user.role, existing.role);
}
