use crate::crud::all::ExpectedResponse;
use crate::fakers::all::{fake_create_schedule, fake_update_schedule};
use crate::PiggiesWorld;
use cucumber::{given, then, when};
use openapi::apis::default_api::{
    create_schedules, delete_schedules, read_all_schedules, read_user_schedules, update_schedules,
};
use openapi::models::ScheduleResponse;

#[when("admin creates a new schedule")]
async fn schedule_is_created(world: &mut PiggiesWorld) {
    create_new_schedule(world).await;
}

#[given("a schedule exists")]
async fn schedule_exists(world: &mut PiggiesWorld) {
    create_new_schedule(world).await;
}

#[when("admin views all schedules")]
async fn all_schedules(world: &mut PiggiesWorld) {
    let response = read_all_schedules(&world.configuration).await.unwrap();
    world.schedules.set_all(response);
}

#[when("user views her schedules")]
async fn user_schedules(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let response = read_user_schedules(&world.configuration, &user.id)
        .await
        .unwrap();
    world.schedules.set_all(response);
}

#[when("admin updates the schedule")]
async fn update_schedule(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let created = world.schedules.created();
    let id = created.id.clone();
    let request = fake_update_schedule();
    update_schedules(&world.configuration, &id, request.clone())
        .await
        .unwrap();
    let result = world.schedules.create_expected_response(id, user, &request);
    world.schedules.set_updated(result)
}

#[when("admin deletes the schedule")]
async fn delete_schedule(world: &mut PiggiesWorld) {
    let created = world.schedules.created();
    delete_schedules(&world.configuration, &created.id.clone())
        .await
        .unwrap();
}

#[then("the schedule is present")]
async fn schedule_is_present(world: &mut PiggiesWorld) {
    let created = world.schedules.created();
    check_schedule(world, created).await;
}

#[then("the schedule is updated")]
async fn schedule_is_updated(world: &mut PiggiesWorld) {
    let updated = world.schedules.updated();
    check_schedule(world, updated).await;
}

#[then("the schedule is not present")]
async fn schedule_is_not_present(world: &mut PiggiesWorld) {
    let created = world.schedules.created();
    assert_eq!(
        world.schedules.all().iter().find(|v| v.id == created.id),
        None
    );
}

async fn create_new_schedule(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let mut request = fake_create_schedule();
    request.user_id = user.id.clone();
    let id = create_schedules(&world.configuration, request.clone())
        .await
        .unwrap();
    let response = world.schedules.create_expected_response(id, user, &request);
    world.schedules.set_created(response)
}

async fn check_schedule(world: &PiggiesWorld, schedule: &ScheduleResponse) {
    let existing = world
        .schedules
        .all()
        .iter()
        .find(|v| v.id == schedule.id)
        .unwrap();
    assert_eq!(schedule.amount, existing.amount);
    assert_eq!(schedule.frequency, existing.frequency);
    assert_eq!(schedule.user_id, existing.user_id);
    assert_eq!(schedule.name, existing.name);
}
