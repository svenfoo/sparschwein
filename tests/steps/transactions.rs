use crate::crud::all::{admin_user, ExpectedResponse};
use crate::fakers::all::fake_create_transaction;
use crate::PiggiesWorld;
use cucumber::{then, when};
use openapi::apis::default_api::{
    create_transactions, read_all_transactions, read_user_transactions,
};

#[when("admin creates a new transaction")]
async fn transaction_exists(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let mut request = fake_create_transaction();
    request.user_id = user.id.clone();
    let amount = request.amount;
    let id = create_transactions(&world.configuration, request.clone())
        .await
        .unwrap();
    let response = world
        .transactions
        .create_expected_response(id, &admin_user(), &request);
    world.transactions.set_created(response);
    let mut updated = user.clone();
    updated.balance += amount;
    world.users.set_updated(updated);
}

#[when("admin views all transactions")]
async fn all_transactions(world: &mut PiggiesWorld) {
    let response = read_all_transactions(&world.configuration).await.unwrap();
    world.transactions.set_all(response);
}

#[when("user views her transactions")]
async fn user_transactions(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let response = read_user_transactions(&world.configuration, &user.id.clone())
        .await
        .unwrap();
    world.transactions.set_all(response);
}

#[then("the transaction is present")]
async fn transaction_is_present(world: &mut PiggiesWorld) {
    let user = world.users.created();
    let created = world.transactions.created();
    let transaction = world
        .transactions
        .all()
        .iter()
        .find(|v| v.id == created.id)
        .unwrap();
    assert_eq!(transaction.creator_name, created.creator_name);
    assert_eq!(transaction.receiver_name, user.name);
    assert_eq!(transaction.amount, created.amount);
    assert_eq!(transaction.comment, created.comment);
}
