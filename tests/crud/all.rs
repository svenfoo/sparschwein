use chrono::{DateTime, Local};
use openapi::models::{
    CreateSchedule, CreateTransaction, CreateUser, Role, ScheduleResponse, TransactionResponse,
    UpdateSchedule, UpdateUser, UserResponse, Withdraw,
};

pub fn admin_user() -> UserResponse {
    UserResponse {
        id: "admin id".to_string(),
        balance: 0.0,
        name: "Admin".to_string(),
        email: "admin email".to_string(),
        role: Role::Admin,
    }
}

#[derive(Debug, Default)]
pub struct CRUD<T> {
    created: Option<T>,
    updated: Option<T>,
    all: Vec<T>,
}

impl<T> CRUD<T> {
    pub fn set_created(&mut self, created: T) {
        self.created = Some(created)
    }
    pub fn set_updated(&mut self, updated: T) {
        self.updated = Some(updated)
    }
    #[allow(dead_code)]
    pub fn set_all(&mut self, all: Vec<T>) {
        self.all = all
    }
    pub fn created(&self) -> &T {
        self.created.as_ref().expect("No created values")
    }
    pub fn updated(&self) -> &T {
        self.updated.as_ref().expect("No updated values")
    }
    #[allow(dead_code)]
    pub fn all(&self) -> &Vec<T> {
        self.all.as_ref()
    }
}

pub trait ExpectedResponse<Request, Response> {
    fn create_expected_response(
        &mut self,
        id: String,
        user: &UserResponse,
        request: &Request,
    ) -> Response;
}

impl ExpectedResponse<CreateSchedule, ScheduleResponse> for CRUD<ScheduleResponse> {
    fn create_expected_response(
        &mut self,
        id: String,
        user: &UserResponse,
        request: &CreateSchedule,
    ) -> ScheduleResponse {
        ScheduleResponse {
            id,
            frequency: request.frequency,
            amount: request.amount,
            user_id: request.user_id.clone(),
            name: user.name.clone(),
        }
    }
}

impl ExpectedResponse<UpdateSchedule, ScheduleResponse> for CRUD<ScheduleResponse> {
    fn create_expected_response(
        &mut self,
        id: String,
        user: &UserResponse,
        request: &UpdateSchedule,
    ) -> ScheduleResponse {
        ScheduleResponse {
            id,
            frequency: request.frequency,
            amount: request.amount,
            user_id: user.id.clone(),
            name: user.name.clone(),
        }
    }
}

impl ExpectedResponse<CreateTransaction, TransactionResponse> for CRUD<TransactionResponse> {
    fn create_expected_response(
        &mut self,
        id: String,
        user: &UserResponse,
        request: &CreateTransaction,
    ) -> TransactionResponse {
        let local: DateTime<Local> = Local::now();
        let date = local.format("%d.%m.%Y, %H:%M").to_string();
        TransactionResponse {
            id,
            creator_name: Some("Admin".to_string()),
            receiver_name: user.name.clone(),
            amount: request.amount,
            comment: request.comment.clone(),
            created_at: date,
        }
    }
}

impl ExpectedResponse<Withdraw, TransactionResponse> for CRUD<TransactionResponse> {
    fn create_expected_response(
        &mut self,
        id: String,
        user: &UserResponse,
        request: &Withdraw,
    ) -> TransactionResponse {
        let local: DateTime<Local> = Local::now();
        let date = local.format("%d.%m.%Y, %H:%M").to_string();
        TransactionResponse {
            id,
            creator_name: Some(user.name.clone()),
            receiver_name: user.name.clone(),
            amount: request.amount,
            comment: request.comment.clone(),
            created_at: date,
        }
    }
}

impl ExpectedResponse<CreateUser, UserResponse> for CRUD<UserResponse> {
    fn create_expected_response(
        &mut self,
        id: String,
        _: &UserResponse,
        request: &CreateUser,
    ) -> UserResponse {
        UserResponse {
            id,
            balance: 0.0,
            name: request.name.clone(),
            email: request.email.clone(),
            role: request.role,
        }
    }
}

impl ExpectedResponse<UpdateUser, UserResponse> for CRUD<UserResponse> {
    fn create_expected_response(
        &mut self,
        id: String,
        _: &UserResponse,
        request: &UpdateUser,
    ) -> UserResponse {
        UserResponse {
            id,
            balance: 0.0,
            name: request.name.clone(),
            email: request.email.clone(),
            role: request.role,
        }
    }
}
