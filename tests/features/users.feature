Feature: Users
  In order to keep track of their pocket money the users want to view their
  account details and withdraw money from it

  Background:
    Given an authenticated user

  Scenario: User checks her account
    When user views the user
    Then the user is present

  Scenario: User withdraws money, views transactions
    When user withdraws money
    And user views her transactions
    Then the transaction is present

  Scenario: User withdraws money, checks her account
    When user withdraws money
    When user views the user
    Then the user is updated
