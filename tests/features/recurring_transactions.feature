Feature: Administering recurring transactions
  In order to keep track of the amount pocket money given every month or week
  the admins want to administer recurring transactions

  Background:
    Given admin is authenticated
    And a user exists

  Scenario: Admin creates a recurring transaction, admin views it
    When admin creates a new schedule
    And admin views all schedules
    Then the schedule is present

  Scenario: Admin creates a recurring transaction, user views it
    When admin creates a new schedule
    And user is authenticated
    And user views her schedules
    Then the schedule is present

  Scenario: Admin updates a recurring transaction, admin views it
    Given a schedule exists
    When admin updates the schedule
    And admin views all schedules
    Then the schedule is updated

  Scenario: Admin deletes a recurring transaction, admin views it
    Given a schedule exists
    When admin deletes the schedule
    And admin views all schedules
    Then the schedule is not present
