Feature: Administering users
  In order to keep track of the amount pocket money given every month or week
  the admins want to administer users

  Background:
    Given admin is authenticated

  Scenario: Admin creates a user, admin views it
    When admin creates a new user
    And admin views all users
    Then the user is present

  Scenario: Admin updates a user, admin views it
    Given a user exists
    When admin updates the user
    And admin views all users
    Then the user is updated

  Scenario: Admin deletes a user, admin views it
    Given a user exists
    When admin deletes the user
    And admin views all users
    Then the user is not present

  Scenario: Admin views user
    Given a user exists
    When admin views the user
    Then the user is present
