Feature: Single transaction
  In order to make note of money given and taken outside from monthly pocket money
  the admins want to create single transactions and users want to view them

  Background:
    Given admin is authenticated
    And a user exists

  Scenario: Admin creates a single transaction, admin views it
    When admin creates a new transaction
    And admin views all transactions
    Then the transaction is present

  Scenario: Admin creates a single transaction, admin views users
    When admin creates a new transaction
    And admin views all users
    Then the user is updated

  Scenario: Admin creates a single transaction, user views it
    When admin creates a new transaction
    And user is authenticated
    And user views her transactions
    Then the transaction is present