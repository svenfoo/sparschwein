/*
 * sparschwein API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.3.0
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct Labels {
    #[serde(rename = "balance")]
    pub balance: String,
    #[serde(rename = "hello")]
    pub hello: String,
    #[serde(rename = "loading")]
    pub loading: String,
    #[serde(rename = "no_schedules")]
    pub no_schedules: String,
    #[serde(rename = "no_transactions")]
    pub no_transactions: String,
    #[serde(rename = "no_users")]
    pub no_users: String,
    #[serde(rename = "on_error")]
    pub on_error: String,
    #[serde(rename = "on_sign_out")]
    pub on_sign_out: String,
    #[serde(rename = "schedule")]
    pub schedule: String,
}

impl Labels {
    pub fn new(balance: String, hello: String, loading: String, no_schedules: String, no_transactions: String, no_users: String, on_error: String, on_sign_out: String, schedule: String) -> Labels {
        Labels {
            balance,
            hello,
            loading,
            no_schedules,
            no_transactions,
            no_users,
            on_error,
            on_sign_out,
            schedule,
        }
    }
}


