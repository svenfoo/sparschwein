use crate::schema::*;

use chrono::{NaiveDateTime, Utc};
use derive_getters::{Dissolve, Getters};
use diesel::{Identifiable, Insertable, Queryable};
use enums::{Frequency, Role};
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;

#[cfg(test)]
use fake::{
    faker::{
        internet::en::{FreeEmail, Password},
        name::en::FirstName,
    },
    Dummy, Fake, Faker,
};

pub type Money = f64;

pub mod enums {
    #[cfg(test)]
    use fake::Dummy;
    use openapi_type::OpenapiType;
    use serde_derive::{Deserialize, Serialize};

    #[derive(Clone, Copy, Debug, DbEnum, Serialize, Deserialize, OpenapiType)]
    #[cfg_attr(test, derive(Dummy))]
    pub enum Frequency {
        Monthly,
        Weekly,
    }

    impl Frequency {
        pub fn as_cron_expression(&self) -> &'static str {
            match self {
                Self::Monthly => "0 0 0 1 * * *",
                Self::Weekly => "0 0 0 * * Sun *",
            }
        }
    }

    #[derive(Clone, Copy, Debug, PartialEq, Eq, DbEnum, Serialize, Deserialize, OpenapiType)]
    #[cfg_attr(test, derive(Dummy))]
    pub enum Role {
        User,
        Admin,
    }
}

#[derive(Identifiable, Insertable, Queryable, Serialize, Deserialize, Debug, Getters, Dissolve)]
#[cfg_attr(test, derive(Dummy))]
pub struct User {
    id: Uuid,
    #[cfg_attr(test, dummy(faker = "FirstName()"))]
    name: String,
    #[cfg_attr(test, dummy(faker = "FreeEmail()"))]
    email: String,
    role: Role,
    balance: Money,
    is_deleted: bool,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
}

impl User {
    pub fn new(name: String, email: String, role: Role) -> Self {
        Self {
            id: Uuid::new_v4(),
            name,
            email,
            balance: 0.,
            role,
            is_deleted: false,
            created_at: Utc::now().naive_utc(),
            updated_at: Utc::now().naive_utc(),
        }
    }

    #[cfg(test)]
    pub fn update_balance(&mut self, balance: Money) {
        self.balance = balance;
    }

    #[cfg(test)]
    pub fn fake(role: Role) -> User {
        let fake: User = Faker.fake();
        let (_, name, email, _, _, _, _, _) = fake.dissolve();
        User::new(name, email, role)
    }
}

#[derive(Associations, Insertable, Queryable, Debug, Getters)]
#[belongs_to(User)]
#[cfg_attr(test, derive(Dummy))]
pub struct Password {
    user_id: Uuid,
    #[cfg_attr(test, dummy(faker = "Password(6..8)"))]
    password: String,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
}

impl Password {
    pub fn new(user_id: Uuid, password: String) -> Self {
        Self {
            user_id,
            password,
            created_at: Utc::now().naive_utc(),
            updated_at: Utc::now().naive_utc(),
        }
    }
}

#[derive(
    Associations,
    Identifiable,
    Insertable,
    Queryable,
    Serialize,
    Deserialize,
    Debug,
    Getters,
    Dissolve,
)]
#[belongs_to(User)]
#[cfg_attr(test, derive(Dummy))]
pub struct Schedule {
    id: Uuid,
    user_id: Uuid,
    frequency: Frequency,
    amount: Money,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
}

impl Schedule {
    pub fn new(user_id: Uuid, frequency: Frequency, amount: Money) -> Self {
        Self {
            id: Uuid::new_v4(),
            user_id,
            frequency,
            amount,
            created_at: Utc::now().naive_utc(),
            updated_at: Utc::now().naive_utc(),
        }
    }
}

#[derive(Associations, Identifiable, Insertable, Queryable, Serialize, Deserialize, Debug)]
#[belongs_to(User, foreign_key=creator_id)]
#[belongs_to(User, foreign_key=receiver_id)]
#[table_name = "raw_transactions"]
#[cfg_attr(test, derive(Dummy))]
pub struct RawTransaction {
    id: Uuid,
    /// Some(user_id) who initiated the transaction, or None for scheduled transactions
    creator_id: Option<Uuid>,
    receiver_id: Uuid,
    amount: Money,
    comment: String,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
}

impl RawTransaction {
    pub fn new(receiver: Uuid, amount: Money, comment: String, creator: Option<Uuid>) -> Self {
        Self {
            id: Uuid::new_v4(),
            creator_id: creator,
            receiver_id: receiver,
            amount,
            comment,
            created_at: Utc::now().naive_utc(),
            updated_at: Utc::now().naive_utc(),
        }
    }
}

#[derive(Identifiable, Queryable, Serialize, Deserialize, Debug, Getters, Dissolve)]
#[cfg_attr(test, derive(Dummy))]
pub struct Transaction {
    id: Uuid,
    /// Some(user_id) who initiated the transaction, or None for scheduled transactions
    creator_id: Option<Uuid>,
    #[cfg_attr(test, dummy(faker = "FirstName()"))]
    creator_name: Option<String>,
    receiver_id: Uuid,
    #[cfg_attr(test, dummy(faker = "FirstName()"))]
    receiver_name: String,
    amount: Money,
    comment: String,
    created_at: NaiveDateTime,
    updated_at: NaiveDateTime,
}

#[cfg(test)]
mod tests {
    use cron::{Schedule, TimeUnitSpec};
    use std::str::FromStr;

    use super::*;

    #[test]
    fn frequency_monthly_has_valid_cron_expression() {
        let expr = Frequency::Monthly.as_cron_expression();
        let schedule = Schedule::from_str(expr).expect("Failed to parse cron expression");
        assert!(schedule.years().is_all());
        assert!(schedule.months().is_all());
        assert_eq!(schedule.days_of_month().count(), 1);
    }

    #[test]
    fn frequency_weekly_has_valid_cron_expression() {
        let expr = Frequency::Weekly.as_cron_expression();
        let schedule = Schedule::from_str(expr).expect("Failed to parse cron expression");
        assert!(schedule.years().is_all());
        assert!(schedule.months().is_all());
        assert_eq!(schedule.days_of_week().count(), 1);
    }

    #[test]
    fn new_user_is_not_deleted() {
        let user = User::new("test".to_string(), "".to_string(), Role::User);
        assert_eq!(user.is_deleted, false);
    }

    #[test]
    fn new_user_has_zero_balance() {
        let user = User::new("test".to_string(), "".to_string(), Role::User);
        assert_eq!(user.balance, 0.0);
    }
}
