#![allow(unused_imports)]

table! {
    use diesel::sql_types::*;
    use crate::models::enums::*;

    passwords (user_id) {
        user_id -> Uuid,
        password -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::models::enums::*;

    raw_transactions (id) {
        id -> Uuid,
        creator_id -> Nullable<Uuid>,
        receiver_id -> Uuid,
        amount -> Float8,
        comment -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::models::enums::*;

    schedules (id) {
        id -> Uuid,
        user_id -> Uuid,
        frequency -> FrequencyMapping,
        amount -> Float8,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::models::enums::*;

    transactions (id) {
        id -> Uuid,
        creator_id -> Nullable<Uuid>,
        creator_name -> Nullable<Text>,
        receiver_id -> Uuid,
        receiver_name -> Text,
        amount -> Float8,
        comment -> Text,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::models::enums::*;

    users (id) {
        id -> Uuid,
        name -> Varchar,
        email -> Varchar,
        role -> RoleMapping,
        balance -> Float8,
        is_deleted -> Bool,
        created_at -> Timestamp,
        updated_at -> Timestamp,
    }
}

joinable!(passwords -> users (user_id));
joinable!(schedules -> users (user_id));

allow_tables_to_appear_in_same_query!(passwords, raw_transactions, schedules, users,);
