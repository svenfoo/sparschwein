use crate::config::{app_url, jwt_secret};
use crate::resources::auth::AuthData;
use crate::{health, resources};

use diesel::PgConnection;
use gotham::hyper::header::CONTENT_TYPE;
use gotham::hyper::Method;
use gotham::router::builder::{self, DefineSingleRoute, DrawRoutes};
use gotham::router::Router;
use gotham_middleware_diesel::DieselMiddleware;
use gotham_restful::cors::{Headers, Origin};
use gotham_restful::gotham::handler::FileOptions;
use gotham_restful::gotham::middleware::logger::RequestLogger;
use gotham_restful::gotham::pipeline::{new_pipeline, single_pipeline};
use gotham_restful::*;

pub type Repo = gotham_middleware_diesel::Repo<PgConnection>;

static API_URL: &str = "/api/v1";

fn api_router(repo: Repo) -> Router {
    let auth: AuthMiddleware<AuthData, _> = AuthMiddleware::new(
        AuthSource::AuthorizationHeader,
        AuthValidation::default(),
        StaticAuthHandler::from_array(jwt_secret().as_ref()),
    );
    let (chain, pipelines) = single_pipeline(
        new_pipeline()
            .add(DieselMiddleware::new(repo))
            .add(RequestLogger::new(log::Level::Info))
            .add(CorsConfig {
                origin: Origin::Single(app_url()),
                headers: Headers::List(vec![CONTENT_TYPE]),
                max_age: 86400,
                credentials: false,
            })
            .add(auth)
            .build(),
    );
    builder::build_router(chain, pipelines, |route| {
        let info = OpenapiInfo {
            title: format!("{} API", env!("CARGO_PKG_NAME")),
            version: env!("CARGO_PKG_VERSION").to_string(),
            urls: vec![app_url() + API_URL],
        };
        route.with_openapi(info, |mut route| {
            route.resource::<resources::schedule::Resource>("schedules");
            route.resource::<resources::transaction::Resource>("transactions");
            route.resource::<resources::user::Resource>("users");
            route.resource::<resources::translation::Resource>("translations");
            route.resource::<resources::auth::Resource>("auth");
            route.openapi_spec("openapi");
            route.openapi_doc("api_doc");
        });
        for method in [Method::GET, Method::POST, Method::PUT, Method::DELETE] {
            route.cors("/users", method.clone());
            route.cors("/schedules", method.clone());
            route.cors("/transactions", method.clone());
            route.cors("/translations", method.clone());
        }
    })
}

fn health_router(repo: Repo) -> Router {
    let (chain, pipelines) = single_pipeline(
        new_pipeline()
            .add(DieselMiddleware::new(repo))
            .add(RequestLogger::new(log::Level::Info))
            .build(),
    );
    builder::build_router(chain, pipelines, |route| {
        route.get("/").to(health::check);
    })
}

pub fn router(repo: Repo) -> Router {
    builder::build_simple_router(|route| {
        route.delegate(API_URL).to_router(api_router(repo.clone()));
        route.delegate("/api/health").to_router(health_router(repo));

        route.get("/").to_file("frontend/public/index.html");
        route.get("/*").to_file("frontend/public/index.html");
        route
            .get("dist/elm.js")
            .to_file(file_options("frontend/public/dist/elm.js"));
        route
            .get("/logo.svg")
            .to_file(file_options("frontend/public/logo.svg"));
        route
            .get("/DIN.ttf")
            .to_file(file_options("frontend/public/DIN.ttf"));
        route
            .get("/DIN-Bold.ttf")
            .to_file(file_options("frontend/public/DIN-Bold.ttf"));
    })
}

fn file_options(path: &str) -> FileOptions {
    FileOptions::new(path)
        .with_cache_control("public, max-age=604800, immutable")
        .with_gzip(true)
        //.with_brotli(true)
        .build()
}
