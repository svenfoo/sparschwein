#[cfg(test)]
use crate::{config::database_url, error::Error, router::Repo};
use diesel::PgConnection;
use diesel_migrations::RunMigrationsError;
#[cfg(test)]
use dotenv::dotenv;

embed_migrations!();

pub fn run_migrations(output: bool, conn: &PgConnection) -> Result<(), RunMigrationsError> {
    match output {
        true => embedded_migrations::run_with_output(conn, &mut std::io::stdout()),
        false => embedded_migrations::run(conn),
    }
}

#[cfg(test)]
pub async fn run_migrations_and_test_transactions<F>(f: F) -> Repo
where
    F: FnOnce(&PgConnection) + Send + std::marker::Unpin + 'static,
{
    dotenv().ok();
    let repo = Repo::with_test_transactions(&database_url());
    repo.run(move |conn| -> Result<(), Error> {
        run_migrations(false, &conn).expect("Error running migrations");
        f(&conn);
        Ok(())
    })
    .await
    .unwrap();
    repo
}
