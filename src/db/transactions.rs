use crate::db::schedules;
use crate::error::Error;
use crate::models::enums::Frequency;
use crate::models::{RawTransaction, Schedule, Transaction};
use crate::schema::transactions::{created_at, creator_id, receiver_id};
use crate::schema::{raw_transactions, transactions};

use chrono::NaiveDateTime;
use diesel::{BoolExpressionMethods, ExpressionMethods, PgConnection, QueryDsl, RunQueryDsl};
use uuid::Uuid;

pub fn insert(transaction: RawTransaction, conn: &PgConnection) -> Result<Uuid, Error> {
    diesel::insert_into(raw_transactions::dsl::raw_transactions)
        .values(transaction)
        .returning(raw_transactions::dsl::id)
        .get_result(conn)
        .map_err(Error::from)
}

pub fn select(id: Uuid, conn: &PgConnection) -> Result<Transaction, Error> {
    transactions::dsl::transactions
        .find(id)
        .get_result(conn)
        .map_err(Error::from)
}

pub fn select_all(conn: &PgConnection) -> Result<Vec<Transaction>, Error> {
    transactions::dsl::transactions
        .order(transactions::dsl::created_at.desc())
        .load(conn)
        .map_err(Error::from)
}

pub fn select_where_receiver(id: Uuid, conn: &PgConnection) -> Result<Vec<Transaction>, Error> {
    transactions::dsl::transactions
        .filter(transactions::dsl::receiver_id.eq(id))
        .order(transactions::dsl::created_at.desc())
        .load(conn)
        .map_err(Error::from)
}

pub fn execute_transaction(
    receiver: Uuid,
    amount: f64,
    comment: String,
    creator: Option<Uuid>,
    conn: &PgConnection,
) -> Result<Uuid, Error> {
    let transaction = RawTransaction::new(receiver, amount, comment, creator);
    insert(transaction, conn)
}

pub fn execute_scheduled_transaction(
    schedule: &Schedule,
    when: Option<NaiveDateTime>,
    conn: &PgConnection,
) -> Result<usize, Error> {
    let transaction_id = execute_transaction(
        *schedule.user_id(),
        *schedule.amount(),
        "".to_string(),
        None,
        conn,
    )?;
    if let Some(date) = when {
        update_created_at(&transaction_id, date, conn)?;
    };
    Ok(1)
}

pub fn execute_scheduled_transactions(
    freq: Frequency,
    conn: &PgConnection,
) -> Result<usize, Error> {
    let schedules = schedules::select_where_frequency(freq, conn)?;
    let transaction_count = schedules
        .into_iter()
        .filter_map(|schedule| execute_scheduled_transaction(&schedule, None, conn).ok())
        .count();
    Ok(transaction_count)
}

pub fn update_created_at(
    transaction_id: &Uuid,
    when: NaiveDateTime,
    conn: &PgConnection,
) -> Result<usize, Error> {
    diesel::update(raw_transactions::dsl::raw_transactions.find(transaction_id))
        .set(raw_transactions::dsl::created_at.eq(when))
        .execute(conn)
        .map_err(Error::from)
}

pub fn time_of_last_scheduled_transaction(
    receiver: &Uuid,
    conn: &PgConnection,
) -> Option<NaiveDateTime> {
    transactions::dsl::transactions
        .filter(creator_id.is_null().and(receiver_id.eq(receiver)))
        .order(transactions::dsl::created_at.desc())
        .select(created_at)
        .first(conn)
        .ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::db::migrations::run_migrations_and_test_transactions;
    use crate::db::users;
    use crate::models::enums::Role;
    use crate::models::{Money, Password, User};
    use fake::{Fake, Faker};
    use proptest::prelude::*;

    fn random_amount() -> Money {
        let cents = Faker.fake::<i32>();
        cents as f64 / 100.0
    }

    fn arb_money() -> impl Strategy<Value = Money> {
        any::<i32>().prop_map(|v| v as f64 / 100.)
    }

    proptest! {
        #![proptest_config(ProptestConfig::with_cases(10))]
        #[test]
        fn test_that_balance_is_correct(balance in arb_money(), amount in arb_money()) {
            let runtime = tokio::runtime::Runtime::new().unwrap();
            runtime.block_on(async {
                check_that_balance_is_correct(balance, amount).await;
            });
        }
    }

    async fn check_that_balance_is_correct(balance: Money, amount: Money) {
        run_migrations_and_test_transactions(move |conn| {
            let mut initial_user = User::fake(Role::User);
            initial_user.update_balance(balance);
            let password: Password = Faker.fake();
            let user_id =
                users::insert(initial_user, password.password(), conn).expect("Can not create");

            let transaction = RawTransaction::new(user_id, amount, "".to_string(), None);
            insert(transaction, conn).expect("Can not create");

            let user = users::select(user_id, conn).expect("Can not find");
            assert_eq!(balance + amount, *user.balance())
        })
        .await;
    }

    #[tokio::test]
    async fn insert_adds_correct_transaction() {
        run_migrations_and_test_transactions(|conn| {
            let admin = User::fake(Role::Admin);
            let admin_name = admin.name().clone();
            let password: Password = Faker.fake();
            let admin_id = users::insert(admin, password.password(), conn).expect("Can not create");

            let user = User::fake(Role::User);
            let user_name = user.name().clone();
            let password: Password = Faker.fake();
            let user_id = users::insert(user, password.password(), conn).expect("Can not create");

            let amount = random_amount();
            let comment = "Sweet";
            let transaction =
                RawTransaction::new(user_id, amount, comment.to_string(), Some(admin_id));
            let transaction_id = insert(transaction, conn).expect("Can not create");

            let transaction = select(transaction_id, conn).expect("Can not find");
            assert_eq!(transaction.id(), &transaction_id);
            assert_eq!(transaction.receiver_id(), &user_id);
            assert_eq!(transaction.receiver_name(), &user_name);
            assert_eq!(transaction.creator_id(), &Some(admin_id));
            assert_eq!(transaction.creator_name(), &Some(admin_name));
            assert_eq!(transaction.amount(), &amount);
            assert_eq!(transaction.comment(), comment);
        })
        .await;
    }
}
