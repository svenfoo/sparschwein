use crate::error::Error;
use crate::models::{enums::Frequency, Money, Schedule, User};
use crate::schema::{schedules, users};

use diesel::{ExpressionMethods, PgConnection, QueryDsl, RunQueryDsl};
use uuid::Uuid;

pub fn delete(id: Uuid, conn: &PgConnection) -> Result<usize, Error> {
    diesel::delete(schedules::dsl::schedules.find(id))
        .execute(conn)
        .map_err(Error::from)
}

pub fn insert(schedule: Schedule, conn: &PgConnection) -> Result<Uuid, Error> {
    diesel::insert_into(schedules::dsl::schedules)
        .values(schedule)
        .returning(schedules::dsl::id)
        .get_result(conn)
        .map_err(Error::from)
}

pub fn select(id: Uuid, conn: &PgConnection) -> Result<(Schedule, User), Error> {
    schedules::dsl::schedules
        .find(id)
        .inner_join(users::dsl::users)
        .get_result(conn)
        .map_err(Error::from)
}

pub fn select_all(conn: &PgConnection) -> Result<Vec<(Schedule, User)>, Error> {
    schedules::dsl::schedules
        .inner_join(users::dsl::users)
        .order((schedules::dsl::amount.desc(), users::dsl::name))
        .load(conn)
        .map_err(Error::from)
}

pub fn select_where_frequency(
    freq: Frequency,
    conn: &PgConnection,
) -> Result<Vec<Schedule>, Error> {
    schedules::dsl::schedules
        .filter(schedules::dsl::frequency.eq(freq))
        .load::<Schedule>(conn)
        .map_err(Error::from)
}

pub fn select_where_user(id: Uuid, conn: &PgConnection) -> Result<Vec<(Schedule, User)>, Error> {
    schedules::dsl::schedules
        .inner_join(users::dsl::users)
        .filter(schedules::dsl::user_id.eq(id))
        .order((schedules::dsl::amount.desc(), users::dsl::name))
        .load(conn)
        .map_err(Error::from)
}

pub fn update(
    id: Uuid,
    frequency: Frequency,
    amount: Money,
    conn: &PgConnection,
) -> Result<usize, Error> {
    diesel::update(schedules::dsl::schedules.find(id))
        .set((
            schedules::dsl::frequency.eq(frequency),
            schedules::dsl::amount.eq(amount),
        ))
        .execute(conn)
        .map_err(Error::from)
}
