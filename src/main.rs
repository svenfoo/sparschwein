use sparschwein::config::{app_address, database_url, load_and_validate_env_vars};
use sparschwein::db::migrations::run_migrations;
use sparschwein::db::users::create_admin_account_if_not_present;
use sparschwein::router::router;
use sparschwein::scheduler;

use diesel_migrations::embed_migrations;
use env_logger::{Env, Target};
use futures::prelude::*;
use gotham_middleware_diesel::Repo;
use log::{error, info};
use sparschwein::scheduler::execute_missed_scheduled_transactions;

#[macro_use]
extern crate diesel_migrations;

embed_migrations!();

#[tokio::main]
async fn main() {
    load_and_validate_env_vars();

    // By default it logs to stderr, using stdout instead
    env_logger::Builder::from_env(Env::default().default_filter_or("warn"))
        .target(Target::Stdout)
        .init();

    let repo = Repo::new(database_url().as_str());
    repo.run(|conn| run_migrations(true, &conn))
        .await
        .expect("Error running migrations");
    repo.run(|conn| create_admin_account_if_not_present(&conn))
        .await
        .expect("Error creating the admin account");
    repo.run(|conn| execute_missed_scheduled_transactions(&conn))
        .await
        .expect("Error running missed schedules");

    let scheduler = scheduler::init_scheduler(repo.clone()).await;

    let server = gotham::init_server(app_address(), router(repo.clone()));

    if let Err(e) = scheduler.start().await {
        error!("Error on scheduler {:?}", e);
    }
    tokio::select! {
        _ = server.boxed() => { panic!("server finished"); },
        _ = tokio::signal::ctrl_c() => { info!("ctrl-c pressed"); },
    }

    info!("Shutting down gracefully");
}
