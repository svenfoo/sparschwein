use crate::db::{schedules, transactions};
use crate::error::Error;
use crate::models::enums::Frequency;
use chrono::{DateTime, TimeZone, Utc};
use cron::Schedule as CronSchedule;
use diesel::PgConnection;
use log::info;
use std::str::FromStr;
use tokio_cron_scheduler::{Job, JobScheduler};

pub type Repo = gotham_middleware_diesel::Repo<PgConnection>;

pub async fn init_scheduler(repo: Repo) -> JobScheduler {
    let scheduler = JobScheduler::new().await.unwrap();
    scheduler
        .add(cron_job(Frequency::Monthly, repo.clone()))
        .await
        .unwrap();
    scheduler
        .add(cron_job(Frequency::Weekly, repo))
        .await
        .unwrap();
    scheduler
}

fn cron_job(freq: Frequency, repo: Repo) -> Job {
    Job::new(freq.as_cron_expression(), move |uuid, _| {
        info!("Starting {freq:?} transactions with job id {uuid}");
        let repo = repo.clone();
        tokio::spawn(async move {
            let result = repo
                .run(move |conn| -> Result<usize, Error> {
                    transactions::execute_scheduled_transactions(freq, &conn)
                })
                .await
                .unwrap();
            info!("Executed {freq:?} transactions: {result}");
        });
    })
    .unwrap()
}

pub fn execute_missed_scheduled_transactions(conn: &PgConnection) -> Result<usize, Error> {
    let frequencies = vec![Frequency::Weekly, Frequency::Monthly];
    info!("Executing missed schedules");
    let mut count = 0;
    for frequency in frequencies {
        let schedules = schedules::select_where_frequency(frequency, conn)?;
        let expression = frequency.as_cron_expression();
        let cron_schedule = CronSchedule::from_str(expression)?;
        for schedule in schedules {
            let last_date =
                match transactions::time_of_last_scheduled_transaction(schedule.user_id(), conn) {
                    Some(some) => some,
                    None => *schedule.created_at(),
                };
            let date: DateTime<Utc> = Utc.from_local_datetime(&last_date).unwrap();
            for cron_date in cron_schedule.after(&date).filter(|date| date < &Utc::now()) {
                let when = cron_date.naive_utc();
                info!("Executing missed {frequency:?} schedule for {when:?}");
                count += transactions::execute_scheduled_transaction(&schedule, Some(when), conn)?;
            }
        }
    }
    Ok(count)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::db::migrations::run_migrations_and_test_transactions;
    use crate::db::{transactions, users};
    use crate::models::enums::Role;
    use crate::models::{Money, Password, Schedule, User};
    use crate::schema::schedules;
    use chrono::{Datelike, Duration, Months, NaiveDate, Utc, Weekday};
    use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
    use fake::{Fake, Faker};
    use uuid::Uuid;

    fn random_amount() -> Money {
        let cents = Faker.fake::<i32>();
        cents as f64 / 100.0
    }

    fn create_schedule(frequency: Frequency, amount: Money, conn: &PgConnection) -> (Uuid, Uuid) {
        let user = User::fake(Role::User);
        let password: Password = Faker.fake();
        let user_id = users::insert(user, password.password(), conn).expect("Can not create");

        let schedule = Schedule::new(user_id, frequency, amount);
        let schedule_id = crate::db::schedules::insert(schedule, conn).expect("Can not create");
        (schedule_id, user_id)
    }

    #[tokio::test]
    async fn missed_scheduled_transactions_are_executed_when_there_are_no_existing_transactions() {
        run_migrations_and_test_transactions(|conn| {
            let amount = random_amount();
            let (schedule_id, _) = create_schedule(Frequency::Monthly, amount, conn);

            // the schedule was created three months ago
            let month_count = 3;
            let months_ago = Utc::now().date_naive() - Months::new(month_count);
            diesel::update(schedules::dsl::schedules.find(schedule_id))
                .set(schedules::dsl::created_at.eq(months_ago.and_hms_opt(11, 31, 56).unwrap()))
                .execute(conn)
                .expect("Can not update");

            // we missed three scheduled transactions that should be executed now
            let result = execute_missed_scheduled_transactions(conn).expect("Can not create");
            assert_eq!(result, month_count as usize);

            // there should now be three transactions
            let all = transactions::select_all(conn).expect("Can not select");
            assert_eq!(all.len(), month_count as usize);

            // verify if each scheduled transaction has the correct payday
            let mut payday = NaiveDate::from_ymd_opt(months_ago.year(), months_ago.month(), 1).unwrap();
            for transaction in all.iter().rev() {
                payday = payday + Months::new(1);
                assert_eq!(transaction.created_at(), &payday.and_hms_opt(0, 0, 0).unwrap());
                assert_eq!(transaction.amount(), &amount);
            }
        })
        .await;
    }

    #[tokio::test]
    async fn missed_scheduled_transactions_are_executed_when_there_is_an_existing_transaction() {
        run_migrations_and_test_transactions(|conn| {
            let amount = random_amount();
            let (_, user_id) = create_schedule(Frequency::Weekly, amount, conn);

            // the last transaction happened two weeks ago
            let week_count = 2;
            let weeks_ago = Utc::now().naive_utc() - Duration::weeks(week_count);
            let transaction_id =
                transactions::execute_transaction(user_id, amount, "".to_string(), None, conn)
                    .expect("Can not create");
            transactions::update_created_at(&transaction_id, weeks_ago, conn)
                .expect("Can not update");

            // we missed two weekly schedules which should we executed now
            let result = execute_missed_scheduled_transactions(conn).expect("Can not create");
            assert_eq!(result, week_count as usize);

            // overall there should be three transactions now
            let all = transactions::select_all(conn).expect("Can not select");
            assert_eq!(all.len(), 1 + week_count as usize);

            // verify if each scheduled transaction has the correct payday
            let mut payday =
                NaiveDate::from_isoywd_opt(weeks_ago.year(), weeks_ago.iso_week().week(), Weekday::Sun).unwrap();
            // we need the first payday after the last transaction
            if payday == weeks_ago.date() {
                payday += Duration::weeks(1);
            }
            for transaction in all.iter().rev().skip(1) {
                assert_eq!(transaction.created_at(), &payday.and_hms_opt(0, 0, 0).unwrap());
                assert_eq!(transaction.amount(), &amount);
                payday = payday + Duration::weeks(1);
            }
        })
        .await;
    }

    #[tokio::test]
    async fn missed_scheduled_transactions_are_executed_when_there_are_some_existing_transactions()
    {
        run_migrations_and_test_transactions(|conn| {
            let amount = random_amount();
            let (_, user_id) = create_schedule(Frequency::Weekly, amount, conn);

            // there are transactions in the time between two and five weeks ago
            let week_min = 2;
            let week_max = 5;
            let now = Utc::now().naive_utc();
            for week in week_min..(week_min + week_max) {
                let transaction_id =
                    transactions::execute_transaction(user_id, amount, "".to_string(), None, conn)
                        .expect("Can not create");
                transactions::update_created_at(&transaction_id, now - Duration::weeks(week), conn)
                    .expect("Can not update");
            }

            // we missed two weekly schedules which should we executed now
            let result = execute_missed_scheduled_transactions(conn).expect("Can not create");
            assert_eq!(result, week_min as usize);

            // overall it should be seven transactions now
            let all = transactions::select_all(conn).expect("Can not select");
            assert_eq!(all.len(), (week_min + week_max) as usize);
        })
        .await;
    }
}
