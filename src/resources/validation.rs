pub mod schedules {
    pub const MIN_SCHEDULE_AMOUNT: f64 = 0.01;
}

pub mod transactions {
    use validator::ValidationError;

    pub const MIN_TRANSACTION_AMOUNT: f64 = 0.01;

    pub fn validate_amount(value: &f64) -> Result<(), ValidationError> {
        if value > &-MIN_TRANSACTION_AMOUNT && value < &MIN_TRANSACTION_AMOUNT {
            return Err(ValidationError::new("Invalid"));
        }
        Ok(())
    }
}

pub mod users {
    use crate::models::Money;
    use validator::ValidationError;

    pub const MIN_NAME_LENGTH: usize = 1;
    pub const MIN_EMAIL_LENGTH: usize = 6;
    pub const MIN_PASSWORD_LENGTH: usize = 6;
    pub const MAX_WITHDRAW_AMOUNT: f64 = -0.01;

    pub fn validate_against_balance(
        amount: Money,
        args: &Option<f64>,
    ) -> Result<(), ValidationError> {
        if let Some(balance) = args {
            if *balance < 0.0 {
                return Err(ValidationError::new("Already in debt"));
            }
            if -amount > *balance {
                return Err(ValidationError::new("Insufficient balance"));
            }
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use crate::resources::validation::users::validate_against_balance;

    #[test]
    fn withdrawing_validates() {
        let result = validate_against_balance(-4.99, &Some(5.0));
        assert!(result.is_ok())
    }

    #[test]
    fn withdrawing_too_much_does_not_validate() {
        let result = validate_against_balance(-10.0, &Some(5.0));
        assert!(result.is_err())
    }

    #[test]
    fn withdrawing_while_already_in_debt_does_not_validate() {
        let result = validate_against_balance(-10.0, &Some(-20.0));
        assert!(result.is_err())
    }
}
