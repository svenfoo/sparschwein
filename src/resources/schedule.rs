use crate::db::schedules;
use crate::error::Error;
use crate::models::{enums::Frequency, Money, Schedule, User};
use crate::resources::auth::{AuthenticationStatus, Permission};
use crate::resources::validation::schedules::*;

use diesel::PgConnection;
use gotham_restful::*;
use openapi_type::OpenapiType;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;
use validator::{Validate, ValidateArgs, ValidationError};

#[derive(Resource)]
#[resource(create, read, read_all, delete, update)]
pub struct Resource;

#[derive(Deserialize, OpenapiType, Validate)]
struct CreateSchedule {
    #[validate(custom(function = "validate_unique_user", arg = "&'v_a PgConnection"))]
    user_id: Uuid,
    frequency: Frequency,
    #[validate(range(min = "MIN_SCHEDULE_AMOUNT"))]
    amount: f64,
}

#[derive(Deserialize, OpenapiType, Validate)]
struct UpdateSchedule {
    frequency: Frequency,
    #[validate(range(min = "MIN_SCHEDULE_AMOUNT"))]
    amount: f64,
}

#[derive(Debug, Queryable, Serialize, OpenapiType)]
pub struct ScheduleResponse {
    pub id: Uuid,
    pub frequency: Frequency,
    pub amount: Money,
    pub user_id: Uuid,
    pub name: String,
}

#[create]
fn create(
    auth: AuthenticationStatus,
    body: CreateSchedule,
    conn: &PgConnection,
) -> Result<Uuid, Error> {
    auth.ok().admin()?;
    body.validate_args(conn)?;
    let schedule = Schedule::new(body.user_id, body.frequency, body.amount);
    schedules::insert(schedule, conn)
}

fn validate_unique_user(user_id: &Uuid, conn: &PgConnection) -> Result<(), ValidationError> {
    let existing = schedules::select_where_user(*user_id, conn)?;
    match existing.is_empty() {
        true => Ok(()),
        false => Err(ValidationError::new("schedule_exists")),
    }
}

impl From<(Schedule, User)> for ScheduleResponse {
    fn from(tuple: (Schedule, User)) -> Self {
        let (id, user_id, frequency, amount, _, _) = tuple.0.dissolve();
        let (uid, name, _, _, _, _, _, _) = tuple.1.dissolve();
        debug_assert_eq!(user_id, uid);
        ScheduleResponse {
            id,
            frequency,
            amount,
            user_id,
            name,
        }
    }
}

#[read]
fn read(
    auth: AuthenticationStatus,
    id: Uuid,
    conn: &PgConnection,
) -> Result<ScheduleResponse, Error> {
    auth.ok().admin()?;
    schedules::select(id, conn).map(ScheduleResponse::from)
}

#[read_all]
fn read_all(
    auth: AuthenticationStatus,
    conn: &PgConnection,
) -> Result<Vec<ScheduleResponse>, Error> {
    auth.ok().admin()?;
    let result = schedules::select_all(conn)?;
    Ok(result.into_iter().map(ScheduleResponse::from).collect())
}

#[update]
fn update(
    auth: AuthenticationStatus,
    id: Uuid,
    body: UpdateSchedule,
    conn: &PgConnection,
) -> Result<NoContent, Error> {
    auth.ok().admin()?;
    body.validate()?;
    schedules::update(id, body.frequency, body.amount, conn).map(|_| NoContent::default())
}

#[delete]
fn delete(auth: AuthenticationStatus, id: Uuid, conn: &PgConnection) -> Result<NoContent, Error> {
    auth.ok().admin()?;
    schedules::delete(id, conn).map(|_| NoContent::default())
}

#[cfg(test)]
mod tests {
    use super::*;

    impl UpdateSchedule {
        fn test(amount: Money) -> Self {
            UpdateSchedule {
                frequency: Frequency::Weekly,
                amount,
            }
        }
    }

    #[test]
    fn positive_amount_validates() {
        let update = UpdateSchedule::test(1.0);
        assert!(update.validate().is_ok());
    }

    #[test]
    fn zero_amount_does_not_validate() {
        let update = UpdateSchedule::test(0.0);
        assert!(update.validate().is_err());
    }

    #[test]
    fn negative_amount_does_not_validate() {
        let update = UpdateSchedule::test(-1.0);
        assert!(update.validate().is_err());
    }
}
