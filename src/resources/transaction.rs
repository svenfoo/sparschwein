use crate::db::{transactions, transactions::execute_transaction};
use crate::error::Error;
use crate::models::{Money, Transaction};
use crate::resources::auth::{AuthenticationStatus, Permission};

use crate::resources::validation::transactions::validate_amount;
use chrono::NaiveDateTime;
use derive_getters::Dissolve;
use diesel::PgConnection;
use gotham_restful::*;
use openapi_type::OpenapiType;
use serde_derive::{Deserialize, Serialize};
use uuid::Uuid;
use validator::Validate;

#[derive(Resource)]
#[resource(create, read, read_all)]
pub struct Resource;

#[derive(Deserialize, OpenapiType, Validate, Dissolve)]
pub struct CreateTransaction {
    user_id: Uuid,
    #[validate(custom(function = "validate_amount"))]
    amount: Money,
    comment: String,
}

#[cfg(test)]
impl CreateTransaction {
    pub fn new(user_id: Uuid, amount: Money, comment: String) -> Self {
        Self {
            user_id,
            amount,
            comment,
        }
    }
}

#[derive(Serialize, OpenapiType)]
pub struct TransactionResponse {
    id: Uuid,
    creator_name: Option<String>,
    receiver_name: String,
    amount: Money,
    comment: String,
    created_at: NaiveDateTime,
}

impl From<Transaction> for TransactionResponse {
    fn from(transaction: Transaction) -> Self {
        let (id, _, creator_name, _, receiver_name, amount, comment, created_at, _) =
            transaction.dissolve();
        Self {
            id,
            creator_name,
            receiver_name,
            amount,
            comment,
            created_at,
        }
    }
}

#[create]
fn create(
    auth: AuthenticationStatus,
    body: CreateTransaction,
    conn: &PgConnection,
) -> Result<Uuid, Error> {
    let creator = auth.ok().admin()?;
    body.validate()?;
    execute_transaction(body.user_id, body.amount, body.comment, Some(creator), conn)
}

#[read]
fn read(
    auth: AuthenticationStatus,
    id: Uuid,
    conn: &PgConnection,
) -> Result<TransactionResponse, Error> {
    auth.ok().admin()?;
    transactions::select(id, conn).map(TransactionResponse::from)
}

#[read_all]
fn read_all(
    auth: AuthenticationStatus,
    conn: &PgConnection,
) -> Result<Vec<TransactionResponse>, Error> {
    auth.ok().admin()?;
    let result = transactions::select_all(conn)?;
    Ok(result.into_iter().map(TransactionResponse::from).collect())
}

#[cfg(test)]
mod tests {
    use super::*;

    impl CreateTransaction {
        fn test(amount: Money) -> Self {
            CreateTransaction {
                user_id: Uuid::new_v4(),
                amount,
                comment: "".to_string(),
            }
        }
    }

    #[test]
    fn positive_amount_validates() {
        let update = CreateTransaction::test(1.0);
        assert!(update.validate().is_ok());
    }

    #[test]
    fn zero_amount_does_not_validate() {
        let update = CreateTransaction::test(0.0);
        assert!(update.validate().is_err());
    }

    #[test]
    fn negative_amount_does_validate() {
        let update = CreateTransaction::test(-1.0);
        assert!(update.validate().is_ok());
    }
}
