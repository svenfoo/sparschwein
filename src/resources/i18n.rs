use openapi_type::OpenapiType;
use serde_derive::Deserialize;
use serde_derive::Serialize;

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, OpenapiType)]
pub struct Translation {
    pub buttons: Buttons,
    pub titles: Titles,
    pub dialogs: Dialogs,
    pub forms: Forms,
    pub labels: Labels,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, OpenapiType)]
pub struct Buttons {
    pub cancel: String,
    pub confirm: String,
    pub delete: String,
    pub edit: String,
    pub new_schedule: String,
    pub new_transaction: String,
    pub new_user: String,
    pub sign_in: String,
    pub sign_out: String,
    pub submit: String,
    pub view: String,
    pub withdraw: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, OpenapiType)]
pub struct Titles {
    pub home: String,
    pub name: String,
    pub schedules: String,
    pub transactions: String,
    pub users: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, OpenapiType)]
pub struct Dialogs {
    pub confirm_title: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, OpenapiType)]
pub struct Forms {
    pub account: String,
    pub amount: String,
    pub comment: String,
    pub edit_user: String,
    pub edit_schedule: String,
    pub email: String,
    pub frequency: String,
    pub frequency_monthly: String,
    pub frequency_weekly: String,
    pub name: String,
    pub on_account_empty: String,
    pub on_amount_empty: String,
    pub on_email_empty: String,
    pub on_length_less_than: String,
    pub on_name_empty: String,
    pub on_not_a_number: String,
    pub on_number_less_than: String,
    pub on_number_outside: String,
    pub on_password_empty: String,
    pub password: String,
    pub role: String,
    pub role_admin: String,
    pub role_user: String,
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize, OpenapiType)]
pub struct Labels {
    pub balance: String,
    pub hello: String,
    pub loading: String,
    pub no_schedules: String,
    pub no_transactions: String,
    pub no_users: String,
    pub on_error: String,
    pub on_sign_out: String,
    pub schedule: String,
}
