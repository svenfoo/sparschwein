use crate::schema::users;

use diesel::{PgConnection, QueryDsl, QueryResult, RunQueryDsl};
use futures::FutureExt;
use gotham::handler::HandlerFuture;
use gotham::helpers::http::response::create_response;
use gotham::hyper::{Body, Response, StatusCode};
use gotham::state::{FromState, State};
use gotham_middleware_diesel::Repo;
use serde::Serialize;
use std::pin::Pin;
use std::time::Duration;

#[derive(Serialize)]
#[serde(rename_all = "lowercase")]
pub enum Status {
    Pass,
    Fail,
    Warn,
}

#[derive(Serialize)]
pub struct Health {
    status: Status,
}

impl From<Status> for Health {
    fn from(status: Status) -> Self {
        Self { status }
    }
}

impl Health {
    fn as_response(&self, state: &State) -> Response<Body> {
        let body = serde_json::to_string(self).expect("serialization failed");
        let mime = "application/health+json"
            .parse()
            .expect("invalid MIME type");
        create_response(state, StatusCode::OK, mime, body)
    }
}

fn db_status(conn: &PgConnection) -> QueryResult<Status> {
    users::dsl::users.count().get_result(conn).map_or_else(
        |_| Ok(Status::Fail),
        |count: i64| match count {
            0 => Ok(Status::Fail), // admin user doesn't exist
            _ => Ok(Status::Pass),
        },
    )
}

pub fn check(state: State) -> Pin<Box<HandlerFuture>> {
    let repo = Repo::<PgConnection>::borrow_from(&state).clone();
    async move {
        let result: QueryResult<Status> =
            tokio::time::timeout(Duration::from_secs(2), repo.run(|conn| db_status(&conn)))
                .await
                .unwrap_or(Ok(Status::Fail));
        match result {
            Ok(status) => {
                let response = Health::from(status).as_response(&state);
                Ok((state, response))
            }
            Err(e) => Err((state, e.into())),
        }
    }
    .boxed()
}

#[cfg(test)]
mod tests {
    use crate::db::migrations::run_migrations_and_test_transactions;
    use crate::db::users::create_admin_account_if_not_present;
    use crate::router::router;
    use crate::schema::users;
    use diesel::RunQueryDsl;
    use gotham::{hyper::StatusCode, plain::test::AsyncTestServer};

    async fn check_health_response(user_exists: bool, expected_body: &str) {
        let repo = run_migrations_and_test_transactions(move |conn| {
            diesel::delete(users::dsl::users)
                .execute(conn)
                .expect("Can not delete");
            if user_exists {
                create_admin_account_if_not_present(conn).expect("Can not create admin");
            }
        })
        .await;

        let test_server = AsyncTestServer::new(router(repo))
            .await
            .expect("Can not start test server");
        let client = test_server.client();
        let request = client.get("http://localhost:3000/api/health");
        let response = request.perform().await.expect("Can not send a request");
        assert_eq!(response.status(), StatusCode::OK);
        let body = response
            .read_utf8_body()
            .await
            .expect("Can not read the UTF-8 encoded response body");
        assert_eq!(body, expected_body);
    }

    #[tokio::test]
    async fn status_is_fail_if_no_users_exist() {
        check_health_response(false, r#"{"status":"fail"}"#).await;
    }

    #[tokio::test]
    async fn status_is_pass_if_at_least_one_user_exists() {
        check_health_response(true, r#"{"status":"pass"}"#).await;
    }
}
