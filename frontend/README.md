# Frontend

> 🌳  built with [elm-spa](https://elm-spa.dev)

## Install

```
yarn
```

## Generate OpenAPI client

```
yarn generate
```

## Run

```
yarn dev
```

## Format

```
yarn format
```

## i18n

After updating translations, run

```
yarn i18n
```
