module Forms.ScheduleForm exposing (EditSchedule, NewSchedule, defaultNew, editForm, editScheduleValidator, newForm, newScheduleValidator, stringFromFrequency)

import Api.Data exposing (Frequency(..), UserResponse, frequencyVariants)
import Element exposing (Element)
import Forms.Fields exposing (amountField)
import Forms.Validators exposing (ValidationField, amountValidator, userValidator)
import I18Next exposing (Translations)
import Translations.Buttons exposing (newSchedule)
import Translations.Forms exposing (frequency, frequencyMonthly, frequencyWeekly)
import UI.Card exposing (viewForm)
import UI.Checkbox exposing (CheckBox, viewCheckBox)
import Uuid exposing (Uuid)
import Validate exposing (Validator)


type alias NewSchedule =
    { user : Maybe UserResponse
    , amount : String
    , frequency : Frequency
    , errors : List ( ValidationField, String )
    }


type alias EditSchedule =
    { id : Uuid
    , title : String
    , frequency : Frequency
    , amount : String
    , errors : List ( ValidationField, String )
    }


defaultNew : NewSchedule
defaultNew =
    { frequency = FrequencyMonthly, amount = "", user = Nothing, errors = [] }


newForm : Translations -> NewSchedule -> Element a -> (NewSchedule -> a) -> a -> a -> Element a
newForm translations schedule dropdown onEdit onCancel onSubmit =
    let
        body : List (Element a)
        body =
            [ dropdown
            , amountField translations schedule onEdit onSubmit
            , viewCheckBox (checkBox translations schedule onEdit)
            ]
    in
    viewForm translations (newSchedule translations) body (Just onCancel) onSubmit


editForm : Translations -> EditSchedule -> (EditSchedule -> a) -> a -> a -> Element a
editForm translations schedule onEdit onCancel onSubmit =
    let
        body : List (Element a)
        body =
            [ amountField translations schedule onEdit onSubmit
            , viewCheckBox (checkBox translations schedule onEdit)
            ]
    in
    viewForm translations schedule.title body (Just onCancel) onSubmit


checkBox : Translations -> { a | frequency : Frequency } -> ({ a | frequency : Frequency } -> msg) -> CheckBox msg
checkBox translations schedule onEdit =
    { title = frequency translations
    , selected = stringFromFrequency translations schedule.frequency
    , options = List.map (stringFromFrequency translations) frequencyVariants
    , onChange = \input -> onEdit { schedule | frequency = frequencyFromString translations input }
    }


stringFromFrequency : Translations -> Frequency -> String
stringFromFrequency translations model =
    case model of
        FrequencyMonthly ->
            frequencyMonthly translations

        FrequencyWeekly ->
            frequencyWeekly translations


frequencyFromString : Translations -> String -> Frequency
frequencyFromString translations frequency =
    if frequencyWeekly translations == frequency then
        FrequencyWeekly

    else
        FrequencyMonthly


newScheduleValidator : Translations -> Validator ( ValidationField, String ) NewSchedule
newScheduleValidator translations =
    Validate.all
        [ userValidator translations
        , amountValidator translations
        ]


editScheduleValidator : Translations -> Validator ( ValidationField, String ) EditSchedule
editScheduleValidator translations =
    Validate.all
        [ amountValidator translations
        ]
