module Forms.Validators exposing (ValidationField(..), amountValidator, currentPasswordValidator, emailValidator, nameValidator, passwordValidator, transactionAmountValidator, userValidator)

import I18Next exposing (Translations)
import Translations.Forms exposing (onAccountEmpty, onAmountEmpty, onEmailEmpty, onLengthLessThan, onNameEmpty, onNotANumber, onNumberLessThan, onNumberOutside, onPasswordEmpty)
import Validate exposing (Validator, ifBlank, ifFalse, ifNothing)


type ValidationField
    = Name
    | Email
    | Password
    | User
    | Amount


nameValidator : Translations -> Validator ( ValidationField, String ) { a | name : String }
nameValidator translations =
    Validate.firstError
        [ ifBlank .name ( Name, onNameEmpty translations )
        , ifNotMinimumLength .name 1 ( Name, onLengthLessThan translations "1" )
        ]


emailValidator : Translations -> Validator ( ValidationField, String ) { a | email : String }
emailValidator translations =
    Validate.firstError
        [ ifBlank .email ( Email, onEmailEmpty translations )
        , ifNotMinimumLength .email 6 ( Email, onLengthLessThan translations "6" )
        ]


currentPasswordValidator : Translations -> Validator ( ValidationField, String ) { a | password : String }
currentPasswordValidator translations =
    ifBlank .password ( Password, onPasswordEmpty translations )


passwordValidator : Translations -> Validator ( ValidationField, String ) { a | password : String }
passwordValidator translations =
    Validate.firstError
        [ ifBlank .password ( Password, onPasswordEmpty translations )
        , ifNotMinimumLength .password 6 ( Password, onLengthLessThan translations "6" )
        ]


userValidator : Translations -> Validator ( ValidationField, String ) { a | user : Maybe b }
userValidator translations =
    ifNothing .user ( User, onAccountEmpty translations )


amountValidator : Translations -> Validator ( ValidationField, String ) { a | amount : String }
amountValidator translations =
    Validate.firstError
        [ ifBlank .amount ( Amount, onAmountEmpty translations )
        , ifNotNumber .amount ( Amount, onNotANumber translations )
        , ifNotMinimum .amount 0.01 ( Amount, onNumberLessThan translations "0.01" )
        ]


transactionAmountValidator : Translations -> Validator ( ValidationField, String ) { a | amount : String }
transactionAmountValidator translations =
    Validate.firstError
        [ ifBlank .amount ( Amount, onAmountEmpty translations )
        , ifNotNumber .amount ( Amount, onNotANumber translations )
        , ifNotOutside .amount -0.01 0.01 ( Amount, onNumberOutside translations "-0.01" "0.01" )
        ]


ifNotNumber : (subject -> String) -> error -> Validator error subject
ifNotNumber subjectToString error =
    ifFalse (\subject -> isFloat (subjectToString subject)) error


isFloat : String -> Bool
isFloat str =
    case String.toFloat str of
        Just _ ->
            True

        Nothing ->
            False


ifNotMinimum : (subject -> String) -> Float -> error -> Validator error subject
ifNotMinimum subjectToString min error =
    ifFalse (\subject -> isMinimum (subjectToString subject) min) error


ifNotOutside : (subject -> String) -> Float -> Float -> error -> Validator error subject
ifNotOutside subjectToString min max error =
    ifFalse (\subject -> isOutside (subjectToString subject) min max) error


isOutside : String -> Float -> Float -> Bool
isOutside str min max =
    case String.toFloat str of
        Just some ->
            some < min || some > max

        Nothing ->
            False


isMinimum : String -> Float -> Bool
isMinimum str min =
    case String.toFloat str of
        Just some ->
            some > min

        Nothing ->
            False


ifNotMinimumLength : (subject -> String) -> Int -> error -> Validator error subject
ifNotMinimumLength subjectToString min error =
    ifFalse (\subject -> isMinimumLength (subjectToString subject) min) error


isMinimumLength : String -> Int -> Bool
isMinimumLength str min =
    String.length str >= min
