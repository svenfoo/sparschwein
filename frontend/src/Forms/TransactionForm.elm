module Forms.TransactionForm exposing (NewTransaction, defaultNew, newForm, newTransactionValidator, withdrawForm, withdrawValidator)

import Api.Data exposing (UserResponse)
import Element exposing (Element)
import Forms.Fields exposing (amountField, commentField)
import Forms.Validators exposing (ValidationField, amountValidator, transactionAmountValidator, userValidator)
import I18Next exposing (Translations)
import Translations.Buttons exposing (newTransaction, withdraw)
import UI.Card exposing (viewForm)
import Validate exposing (Validator)


type alias NewTransaction =
    { user : Maybe UserResponse
    , amount : String
    , comment : String
    , errors : List ( ValidationField, String )
    }


defaultNew : NewTransaction
defaultNew =
    { user = Nothing, amount = "", comment = "", errors = [] }


newForm : Translations -> NewTransaction -> Element a -> (NewTransaction -> a) -> a -> a -> Element a
newForm translations transaction dropdown onEdit onCancel onSubmit =
    let
        body : List (Element a)
        body =
            [ dropdown
            , amountField translations transaction onEdit onSubmit
            , commentField translations transaction onEdit onSubmit
            ]
    in
    viewForm translations (newTransaction translations) body (Just onCancel) onSubmit


withdrawForm : Translations -> NewTransaction -> (NewTransaction -> a) -> a -> a -> Element a
withdrawForm translations transaction onEdit onCancel onSubmit =
    let
        body : List (Element a)
        body =
            [ amountField translations transaction onEdit onSubmit
            , commentField translations transaction onEdit onSubmit
            ]
    in
    viewForm translations (withdraw translations) body (Just onCancel) onSubmit


newTransactionValidator : Translations -> Validator ( ValidationField, String ) NewTransaction
newTransactionValidator translations =
    Validate.all
        [ userValidator translations
        , transactionAmountValidator translations
        ]


withdrawValidator : Translations -> Validator ( ValidationField, String ) NewTransaction
withdrawValidator translations =
    amountValidator translations
