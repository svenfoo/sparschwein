module Forms.Fields exposing (amountField, commentField, currentPasswordField, emailField, nameField, passwordField)

import Common exposing (errorsForField)
import Element exposing (Element)
import Forms.Validators exposing (ValidationField(..))
import I18Next exposing (Translations)
import Translations.Forms exposing (amount, comment, email, name, password)
import UI.TextField exposing (currentPasswordTextFieldWithValidation, emailTextFieldWithValidation, textField, textFieldWithValidation)


amountField : Translations -> { a | amount : String, errors : List ( ValidationField, String ) } -> ({ a | amount : String, errors : List ( ValidationField, String ) } -> msg) -> msg -> Element msg
amountField translations schedule onEdit onSubmit =
    textFieldWithValidation
        { title = amount translations
        , initial = schedule.amount
        , onChange = \v -> onEdit { schedule | amount = v }
        , onEnterKey = onSubmit
        , validation = errorsForField Amount schedule.errors
        }


emailField : Translations -> { a | email : String, errors : List ( ValidationField, String ) } -> ({ a | email : String, errors : List ( ValidationField, String ) } -> msg) -> msg -> Element msg
emailField translations user onEdit onSubmit =
    emailTextFieldWithValidation
        { title = email translations
        , initial = user.email
        , onChange = \v -> onEdit { user | email = v }
        , onEnterKey = onSubmit
        , validation = errorsForField Email user.errors
        }


currentPasswordField : Translations -> { a | password : String, errors : List ( ValidationField, String ) } -> ({ a | password : String, errors : List ( ValidationField, String ) } -> msg) -> msg -> Element msg
currentPasswordField translations user onEdit onSubmit =
    currentPasswordTextFieldWithValidation
        { title = password translations
        , initial = user.password
        , onChange = \v -> onEdit { user | password = v }
        , onEnterKey = onSubmit
        , validation = errorsForField Password user.errors
        }


passwordField : Translations -> { a | password : String, errors : List ( ValidationField, String ) } -> ({ a | password : String, errors : List ( ValidationField, String ) } -> msg) -> msg -> Element msg
passwordField translations user onEdit onSubmit =
    textFieldWithValidation
        { title = password translations
        , initial = user.password
        , onChange = \v -> onEdit { user | password = v }
        , onEnterKey = onSubmit
        , validation = errorsForField Password user.errors
        }


nameField : Translations -> { a | name : String, errors : List ( ValidationField, String ) } -> ({ a | name : String, errors : List ( ValidationField, String ) } -> msg) -> msg -> Element msg
nameField translations user onEdit onSubmit =
    textFieldWithValidation
        { title = name translations
        , initial = user.name
        , onChange = \v -> onEdit { user | name = v }
        , onEnterKey = onSubmit
        , validation = errorsForField Name user.errors
        }


commentField : Translations -> { a | comment : String } -> ({ a | comment : String } -> msg) -> msg -> Element msg
commentField translations transaction onEdit onSubmit =
    textField
        { title = comment translations
        , initial = transaction.comment
        , onChange = \v -> onEdit { transaction | comment = v }
        , onEnterKey = onSubmit
        , validation = []
        }
