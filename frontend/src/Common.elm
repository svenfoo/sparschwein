module Common exposing (errorsForField, formatDate, transactionTitle)

import Api.Data exposing (TransactionResponse)
import DateFormat exposing (dayOfMonthFixed, format, hourMilitaryFixed, minuteFixed, monthFixed, text, yearNumber)
import Time


formatDate : Time.Zone -> Time.Posix -> String
formatDate =
    format
        [ dayOfMonthFixed
        , text "."
        , monthFixed
        , text "."
        , yearNumber
        , text ", "
        , hourMilitaryFixed
        , text ":"
        , minuteFixed
        ]


errorsForField : a -> List ( a, String ) -> List String
errorsForField field errors =
    List.map (\( _, error ) -> error) (List.filter (\( fieldError, _ ) -> fieldError == field) errors)


transactionTitle : TransactionResponse -> String
transactionTitle transaction =
    case transaction.creatorName of
        Just some ->
            some ++ " > " ++ transaction.receiverName

        Nothing ->
            transaction.receiverName
