module UI.ColorPalette exposing (black, darkGray, lightGray, red, white)

import Element exposing (Color, rgb255)


white : Color
white =
    rgb255 255 255 255


red : Color
red =
    rgb255 211 47 47


black : Color
black =
    rgb255 0 0 0


darkGray : Color
darkGray =
    rgb255 90 90 90


lightGray : Color
lightGray =
    rgb255 120 120 120
