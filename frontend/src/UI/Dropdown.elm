module UI.Dropdown exposing (Dropdown, initModel, updateModel, usersDropdown)

import Api.Data exposing (UserResponse)
import Element exposing (Element, column, fill, row, spacing, text, width)
import Element.Font as Font
import Element.Input exposing (labelAbove)
import I18Next exposing (Translations)
import SearchBox exposing (ChangeEvent)
import Translations.Forms exposing (account)
import UI.ColorPalette exposing (red)
import UI.Layout exposing (scaled)


type alias Dropdown =
    { user : Maybe UserResponse
    , userText : String
    , userSearchBox : SearchBox.State
    }


initModel : Dropdown
initModel =
    { user = Nothing, userText = "", userSearchBox = SearchBox.init }


updateModel : ChangeEvent UserResponse -> Dropdown -> Dropdown
updateModel changeEvent model =
    case changeEvent of
        SearchBox.SelectionChanged user ->
            { model | user = Just user }

        SearchBox.TextChanged text ->
            { model
                | user = Nothing
                , userText = text
                , userSearchBox = SearchBox.reset model.userSearchBox
            }

        SearchBox.SearchBoxChanged subMsg ->
            { model | userSearchBox = SearchBox.update subMsg model.userSearchBox }


usersDropdown : Translations -> Dropdown -> List UserResponse -> (ChangeEvent UserResponse -> msg) -> List String -> Element msg
usersDropdown translations model users onChange validation =
    column [ width fill, spacing 8 ]
        (row []
            [ SearchBox.input [ width fill ]
                { onChange = onChange
                , text = model.userText
                , selected = model.user
                , options = Just users
                , label = labelAbove [] (text (account translations))
                , placeholder = Nothing
                , toLabel = \user -> user.name
                , filter = \query user -> [ user.name ] |> List.map String.toLower |> List.any (String.contains (String.toLower query))
                , state = model.userSearchBox
                }
            ]
            :: List.map (\v -> row [ Font.size (scaled -1), Font.color red ] [ text v ]) validation
        )
