module UI.Animations exposing (loadingAnimation)

import Element exposing (Attribute, Element, alignBottom, alignTop, centerX, clip, column, el, fill, height, none, padding, px, rgb255, row, spacing, width)
import Element.Background as Background
import Element.Border as Border
import Shared exposing (Window)
import Simple.Animation as Animation exposing (Animation)
import Simple.Animation.Animated as Animated
import Simple.Animation.Property as P
import UI.ColorPalette exposing (darkGray, lightGray)


loadingAnimation : Window -> Element msg
loadingAnimation window =
    column [ clip, alignBottom, centerX ]
        [ row [] [ animatedUBahn window ]
        , row [ width fill ] [ bridge ]
        , row [ width fill, spacing 120 ] (List.repeat (window.width // 120 + 1) bridgePillar)
        ]


animatedUBahn : Window -> Element msg
animatedUBahn window =
    animatedEl (uBahnAnimation window)
        []
        (row [ spacing uBahnSpacing ] (List.repeat uBahnWagons uBahn))


uBahn : Element msg
uBahn =
    el
        [ width (px uBahnWagonWidth)
        , height (px 30)
        , Border.rounded 4
        , Background.color (rgb255 240 215 34)
        ]
        (row [ alignTop, padding 5, spacing 5 ] (List.repeat 5 uBahnWindow))


uBahnWindow : Element msg
uBahnWindow =
    el
        [ width (px 10)
        , height (px 10)
        , Border.rounded 2
        , Background.color darkGray
        ]
        none


uBahnWagonWidth : Int
uBahnWagonWidth =
    80


uBahnSpacing : Int
uBahnSpacing =
    2


uBahnWagons : Int
uBahnWagons =
    5


uBahnWidth : Int
uBahnWidth =
    uBahnWagons * uBahnWagonWidth + (uBahnWagons - 1) * uBahnSpacing


uBahnVelocity : Int
uBahnVelocity =
    6 * 1000


bridge : Element msg
bridge =
    el
        [ width fill
        , height (px 5)
        , Background.color lightGray
        ]
        none


bridgePillar : Element msg
bridgePillar =
    el
        [ width (px 5)
        , height (px 30)
        , Background.color lightGray
        ]
        none


uBahnAnimation : Window -> Animation
uBahnAnimation window =
    Animation.steps
        { startAt = [ P.x -(toFloat uBahnWidth) ]
        , options = [ Animation.loop, Animation.yoyo ]
        }
        [ Animation.step uBahnVelocity [ P.x (toFloat window.width) ]
        ]



-- Elm UI Animated Helpers


animatedEl : Animation -> List (Attribute msg) -> Element msg -> Element msg
animatedEl =
    animatedUi el


animatedUi : (List (Attribute msg) -> children -> Element msg) -> Animation -> List (Attribute msg) -> children -> Element msg
animatedUi =
    Animated.ui
        { behindContent = Element.behindContent
        , htmlAttribute = Element.htmlAttribute
        , html = Element.html
        }
