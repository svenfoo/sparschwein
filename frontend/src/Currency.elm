module Currency exposing (toString)

import FormatNumber exposing (format)
import FormatNumber.Locales exposing (Decimals(..), Locale, frenchLocale)


toString : Float -> String
toString amount =
    format germanLocale amount ++ "€"


germanLocale : Locale
germanLocale =
    { frenchLocale
        | decimals = Exact 2
    }
