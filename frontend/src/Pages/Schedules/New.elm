module Pages.Schedules.New exposing (Model, Msg, State, page)

import Api
import Api.Data exposing (CreateSchedule, UserResponse)
import Api.Request.Default exposing (createSchedules, readAllUsers)
import Auth
import Common exposing (errorsForField)
import Element exposing (Element, text)
import Forms.ScheduleForm exposing (NewSchedule, defaultNew, newForm, newScheduleValidator)
import Forms.Validators exposing (ValidationField(..))
import Gen.Route as Route
import Http
import Page
import Problem exposing (isUnauthenticated)
import Request exposing (Request)
import SearchBox
import Shared
import Storage exposing (Storage)
import Translations.Buttons exposing (newSchedule)
import Translations.Labels exposing (loading, noUsers, onError)
import UI.Dropdown exposing (Dropdown, initModel, updateModel, usersDropdown)
import UI.Layout as Layout
import Uuid exposing (Uuid)
import Validate exposing (Valid, fromValid, validate)
import View exposing (View)


page : Shared.Model -> Request -> Page.With Model Msg
page shared req =
    Page.protected.element
        (\session ->
            { init = init session
            , update = update req shared.storage
            , view = view shared
            , subscriptions = \_ -> Sub.none
            }
        )


type alias Model =
    { session : Auth.User
    , state : State
    , toCreate : NewSchedule
    , usersDropdown : Dropdown
    }


type State
    = Loading
    | Loaded (List UserResponse)
    | Errored String


init : Auth.User -> ( Model, Cmd Msg )
init session =
    loadUsers session


type Msg
    = UsersLoaded (Result Http.Error (List UserResponse))
    | Edit NewSchedule
    | ChangedUserSearchBox (SearchBox.ChangeEvent UserResponse)
    | ClickedCancel
    | ClickedSubmit (Result (List ( ValidationField, String )) (Valid NewSchedule))
    | Created (Result Http.Error Uuid)


update : Request -> Storage -> Msg -> Model -> ( Model, Cmd Msg )
update req storage msg model =
    case msg of
        UsersLoaded (Ok list) ->
            ( { model | state = Loaded list }, Cmd.none )

        UsersLoaded (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        Edit new ->
            ( { model | toCreate = new }, Cmd.none )

        ClickedCancel ->
            ( model, Request.pushRoute Route.Schedules req )

        ClickedSubmit (Ok input) ->
            let
                valid : NewSchedule
                valid =
                    fromValid input
            in
            case ( valid.user, String.toFloat valid.amount ) of
                ( Just user, Just amount ) ->
                    createSchedule model { userId = user.id, frequency = valid.frequency, amount = amount }

                _ ->
                    ( model, Cmd.none )

        ClickedSubmit (Err list) ->
            let
                toCreate : NewSchedule
                toCreate =
                    model.toCreate
            in
            ( { model | toCreate = { toCreate | errors = list } }, Cmd.none )

        Created (Ok _) ->
            ( model, Request.pushRoute Route.Schedules req )

        Created (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        ChangedUserSearchBox changeEvent ->
            let
                toCreate : NewSchedule
                toCreate =
                    model.toCreate
            in
            ( { model | usersDropdown = updateModel changeEvent model.usersDropdown, toCreate = { toCreate | user = model.usersDropdown.user } }, Cmd.none )


view : Shared.Model -> Model -> View Msg
view shared model =
    { title = newSchedule shared.translations
    , body = Layout.layout Route.Schedules__New shared (viewForm shared model)
    }


viewForm : Shared.Model -> Model -> List (Element Msg)
viewForm shared model =
    case model.state of
        Loading ->
            [ text (loading shared.translations) ]

        Loaded users ->
            case users of
                [] ->
                    [ text (noUsers shared.translations) ]

                _ ->
                    let
                        submit : Msg
                        submit =
                            ClickedSubmit (validate (newScheduleValidator shared.translations) model.toCreate)

                        dropdown : Element Msg
                        dropdown =
                            usersDropdown shared.translations model.usersDropdown users ChangedUserSearchBox (errorsForField User model.toCreate.errors)
                    in
                    [ newForm shared.translations model.toCreate dropdown Edit ClickedCancel submit ]

        Errored reason ->
            [ text (onError shared.translations reason) ]


loadUsers : Auth.User -> ( Model, Cmd Msg )
loadUsers session =
    ( { session = session, state = Loading, toCreate = defaultNew, usersDropdown = initModel }, Api.send UsersLoaded (readAllUsers session.token) )


createSchedule : Model -> CreateSchedule -> ( Model, Cmd Msg )
createSchedule model schedule =
    ( { model | state = Loading }, Api.send Created (createSchedules schedule model.session.token) )
