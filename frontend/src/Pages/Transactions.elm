module Pages.Transactions exposing (Model, Msg, State, page)

import Api
import Api.Data exposing (TransactionResponse)
import Api.Request.Default exposing (readAllTransactions)
import Auth
import Common exposing (formatDate, transactionTitle)
import Currency
import Element exposing (Element, text)
import Gen.Route as Route
import Http
import Page
import Problem exposing (isUnauthenticated)
import Request exposing (Request)
import Shared
import Storage exposing (Storage)
import Task
import Time
import Translations.Buttons exposing (newTransaction)
import Translations.Labels exposing (loading, onError)
import Translations.Titles exposing (transactions)
import UI.Button exposing (defaultButton)
import UI.Card exposing (keyedCard)
import UI.Layout as Layout
import View exposing (View)


page : Shared.Model -> Request -> Page.With Model Msg
page shared req =
    Page.protected.element
        (\session ->
            { init = init session
            , update = update req shared.storage
            , view = view shared
            , subscriptions = \_ -> Sub.none
            }
        )


type alias Model =
    { session : Auth.User
    , state : State
    , timeZone : Time.Zone
    }


type State
    = Loading
    | Loaded TransactionList
    | Errored String


type alias TransactionList =
    List TransactionResponse


init : Auth.User -> ( Model, Cmd Msg )
init session =
    loadTransactions session


type Msg
    = TransactionsLoaded (Result Http.Error (List TransactionResponse))
    | AdjustTimeZone Time.Zone
    | ClickedNew


update : Request -> Storage -> Msg -> Model -> ( Model, Cmd Msg )
update req storage msg model =
    case msg of
        TransactionsLoaded (Ok list) ->
            ( { model | state = Loaded list }, Task.perform AdjustTimeZone Time.here )

        TransactionsLoaded (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        AdjustTimeZone zone ->
            ( { model | timeZone = zone }, Cmd.none )

        ClickedNew ->
            ( model, Request.pushRoute Route.Transactions__New req )


view : Shared.Model -> Model -> View Msg
view shared model =
    { title = transactions shared.translations
    , body = Layout.layout Route.Transactions shared (viewTransactions shared model)
    }


viewTransactions : Shared.Model -> Model -> List (Element Msg)
viewTransactions shared model =
    case model.state of
        Loading ->
            [ text (loading shared.translations) ]

        Loaded list ->
            defaultButton (newTransaction shared.translations) ClickedNew :: List.map (viewTransaction model.timeZone) list

        Errored reason ->
            [ text (onError shared.translations reason) ]


viewTransaction : Time.Zone -> TransactionResponse -> Element Msg
viewTransaction timeZone transaction =
    let
        body : List (Element Msg)
        body =
            text (formatDate timeZone transaction.createdAt)
                :: text (Currency.toString transaction.amount)
                :: (case transaction.comment of
                        "" ->
                            []

                        _ ->
                            [ text transaction.comment ]
                   )
    in
    keyedCard { title = transactionTitle transaction, rightLabel = "", body = body, onClick = Nothing, buttons = [] } transaction.id


loadTransactions : Auth.User -> ( Model, Cmd Msg )
loadTransactions session =
    ( { session = session, state = Loading, timeZone = Time.utc }
    , Api.send TransactionsLoaded (readAllTransactions session.token)
    )
