module Pages.Users.Id_.Schedules exposing (Model, Msg, State, page)

import Api
import Api.Data exposing (ScheduleResponse, UserResponse)
import Api.Request.Default exposing (readUserSchedules)
import Auth
import Currency
import Element exposing (Element, text)
import Forms.ScheduleForm exposing (stringFromFrequency)
import Gen.Params.Users.Id_.Schedules exposing (Params)
import Gen.Route as Route
import Http
import Page
import Problem exposing (isUnauthenticated)
import Request
import Shared
import Storage exposing (Storage)
import Translations.Labels exposing (loading, noSchedules, onError)
import Translations.Titles exposing (schedules)
import UI.Card exposing (keyedCard)
import UI.Layout as Layout
import Uuid exposing (Uuid)
import View exposing (View)


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared req =
    Page.protected.element
        (\session ->
            { init = init session req
            , update = update req shared.storage
            , view = view shared
            , subscriptions = \_ -> Sub.none
            }
        )


type alias Model =
    { session : Auth.User
    , id : String
    , state : State
    }


type State
    = Loading
    | Loaded ( List ScheduleResponse, List UserResponse )
    | Errored String


init : Auth.User -> Request.With Params -> ( Model, Cmd Msg )
init session req =
    let
        model : Model
        model =
            { session = session, id = req.params.id, state = Loading }
    in
    case Uuid.fromString req.params.id of
        Just some ->
            loadSchedules session model some

        Nothing ->
            ( model, Request.pushRoute Route.NotFound req )


type Msg
    = SchedulesLoaded (Result Http.Error (List ScheduleResponse))


update : Request.With Params -> Storage -> Msg -> Model -> ( Model, Cmd Msg )
update _ storage msg model =
    case msg of
        SchedulesLoaded (Ok list) ->
            ( { model | state = Loaded ( list, [] ) }, Cmd.none )

        SchedulesLoaded (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )


view : Shared.Model -> Model -> View Msg
view shared model =
    { title = schedules shared.translations
    , body = Layout.layout Route.Schedules shared (viewSchedules shared model)
    }


viewSchedules : Shared.Model -> Model -> List (Element Msg)
viewSchedules shared model =
    case model.state of
        Loading ->
            [ text (loading shared.translations) ]

        Loaded ( schedules, _ ) ->
            case schedules of
                [] ->
                    [ text (noSchedules shared.translations) ]

                _ ->
                    List.map (viewSchedule shared) schedules

        Errored reason ->
            [ text (onError shared.translations reason) ]


viewSchedule : Shared.Model -> ScheduleResponse -> Element Msg
viewSchedule shared schedule =
    let
        body : List (Element Msg)
        body =
            [ text (stringFromFrequency shared.translations schedule.frequency)
            , text (Currency.toString schedule.amount)
            ]
    in
    keyedCard { title = "", rightLabel = "", body = body, onClick = Nothing, buttons = [] } schedule.id


loadSchedules : Auth.User -> Model -> Uuid -> ( Model, Cmd Msg )
loadSchedules _ model id =
    ( { model | state = Loading }, Api.send SchedulesLoaded (readUserSchedules id model.session.token) )
