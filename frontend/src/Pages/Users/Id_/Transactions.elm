module Pages.Users.Id_.Transactions exposing (Datum, Model, Msg, State, page)

import Api
import Api.Data exposing (TransactionResponse)
import Api.Request.Default exposing (readUserTransactions)
import Auth
import Chart as C
import Chart.Attributes as CA
import Chart.Events as CE
import Chart.Item as CI
import Common exposing (formatDate)
import Currency
import Element exposing (Element, el, html, padding, text)
import Gen.Params.Users.Id_.Transactions exposing (Params)
import Gen.Route as Route
import Html exposing (Html, div)
import Html.Attributes exposing (style)
import Http
import List exposing (reverse)
import List.Extra exposing (scanl1)
import Page
import Problem exposing (isUnauthenticated)
import Request
import Shared
import Storage exposing (Storage)
import Task
import Time exposing (posixToMillis)
import Translations.Labels exposing (loading, noTransactions, onError, schedule)
import Translations.Titles exposing (transactions)
import UI.Card exposing (keyedCard)
import UI.Dimensions exposing (fillMaxViewWidth)
import UI.Layout as Layout
import Uuid exposing (Uuid)
import View exposing (View)


page : Shared.Model -> Request.With Params -> Page.With Model Msg
page shared req =
    Page.protected.element
        (\session ->
            { init = init session req
            , update = update req shared.storage
            , view = view shared
            , subscriptions = \_ -> Sub.none
            }
        )


type alias Model =
    { session : Auth.User
    , id : String
    , state : State
    , timeZone : Time.Zone
    , hovering : List (CI.One Datum CI.Dot)
    }


type State
    = Loading
    | Loaded (List TransactionResponse)
    | Errored String


init : Auth.User -> Request.With Params -> ( Model, Cmd Msg )
init session req =
    let
        model : Model
        model =
            { session = session, id = req.params.id, state = Loading, timeZone = Time.utc, hovering = [] }
    in
    case Uuid.fromString req.params.id of
        Just some ->
            loadTransactions session model some

        Nothing ->
            ( model, Request.pushRoute Route.NotFound req )


type Msg
    = TransactionsLoaded (Result Http.Error (List TransactionResponse))
    | AdjustTimeZone Time.Zone
    | OnHover (List (CI.One Datum CI.Dot))


update : Request.With Params -> Storage -> Msg -> Model -> ( Model, Cmd Msg )
update _ storage msg model =
    case msg of
        TransactionsLoaded (Ok list) ->
            ( { model | state = Loaded list }, Task.perform AdjustTimeZone Time.here )

        TransactionsLoaded (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        AdjustTimeZone zone ->
            ( { model | timeZone = zone }, Cmd.none )

        OnHover hovering ->
            ( { model | hovering = hovering }, Cmd.none )


view : Shared.Model -> Model -> View Msg
view shared model =
    { title = transactions shared.translations
    , body = Layout.layout (Route.Users__Id___Transactions { id = model.id }) shared (viewTransactions shared model)
    }


viewTransactions : Shared.Model -> Model -> List (Element Msg)
viewTransactions shared model =
    case model.state of
        Loading ->
            [ text (loading shared.translations) ]

        Loaded list ->
            case list of
                [] ->
                    [ text (noTransactions shared.translations) ]

                _ ->
                    viewChart model (reverse list) :: List.map (viewTransaction shared model.timeZone) list

        Errored reason ->
            [ text (onError shared.translations reason) ]


viewChart : Model -> List TransactionResponse -> Element Msg
viewChart model list =
    el [ fillMaxViewWidth, padding 32 ] (html (chart model list))


viewTransaction : Shared.Model -> Time.Zone -> TransactionResponse -> Element Msg
viewTransaction shared timeZone transaction =
    let
        body : List (Element Msg)
        body =
            text (formatDate timeZone transaction.createdAt)
                :: text (Currency.toString transaction.amount)
                :: (case transaction.comment of
                        "" ->
                            []

                        _ ->
                            [ text transaction.comment ]
                   )
    in
    keyedCard { title = Maybe.withDefault (schedule shared.translations) transaction.creatorName, rightLabel = "", body = body, onClick = Nothing, buttons = [] } transaction.id


chart : Model -> List TransactionResponse -> Html Msg
chart model list =
    C.chart
        [ CA.height 300
        , CA.width 1024
        , CE.onMouseMove OnHover (CE.getNearest CI.dots)
        , CE.onMouseLeave (OnHover [])
        ]
        [ C.xAxis []
        , C.xTicks [ CA.times Time.utc ]
        , C.xLabels [ CA.times Time.utc ]
        , C.yLabels [ CA.withGrid ]
        , C.series .date
            [ C.interpolated .amount
                [ CA.opacity 0.6
                , CA.gradient []
                , CA.color CA.darkYellow
                ]
                [ CA.color "white", CA.borderWidth 1.5, CA.circle ]
            ]
            (datumsFromTransactions list)
        , C.each model.hovering
            (\_ dot ->
                let
                    xValue : Time.Posix
                    xValue =
                        Time.millisToPosix (round (CI.getX dot))

                    yValue : Float
                    yValue =
                        CI.getY dot
                in
                [ C.tooltip dot
                    [ CA.onLeftOrRight, CA.offset 8, CA.center ]
                    []
                    [ chartToolTip (formatDate model.timeZone xValue), chartToolTip (Currency.toString yValue) ]
                ]
            )
        ]


chartToolTip : String -> Html msg
chartToolTip text =
    div [ style "padding" "5px 0 5px 0" ] [ Html.text text ]


type alias Datum =
    { date : Float
    , amount : Float
    }


datumsFromTransactions : List TransactionResponse -> List Datum
datumsFromTransactions transactions =
    let
        balances : List Float
        balances =
            balancesFromTransactions transactions
    in
    List.map2 (\transaction balance -> { date = toFloat (posixToMillis transaction.createdAt), amount = balance }) transactions balances


balancesFromTransactions : List TransactionResponse -> List Float
balancesFromTransactions transactions =
    scanl1 (+) (List.map (\v -> v.amount) transactions)


loadTransactions : Auth.User -> Model -> Uuid -> ( Model, Cmd Msg )
loadTransactions _ model id =
    ( { model | state = Loading }, Api.send TransactionsLoaded (readUserTransactions id model.session.token) )
