module Pages.Home_ exposing (Model, Msg, State, page)

import Api
import Api.Data exposing (Role(..), UserResponse, Withdraw)
import Api.Request.Default exposing (createUserTransactions, readUsers)
import Auth
import Currency
import Element exposing (Element, el, text)
import Element.Font as Font
import Forms.TransactionForm exposing (NewTransaction, defaultNew, withdrawForm, withdrawValidator)
import Forms.Validators exposing (ValidationField)
import Gen.Route as Route
import Http
import Page
import Problem exposing (isUnauthenticated)
import Request exposing (Request)
import Shared
import Storage exposing (Storage)
import Translations.Buttons exposing (withdraw)
import Translations.Labels exposing (balance, hello, loading, onError)
import Translations.Titles exposing (home)
import UI.Button exposing (defaultButton)
import UI.Layout as Layout
import Uuid exposing (Uuid)
import Validate exposing (Valid, fromValid, validate)
import View exposing (View)


page : Shared.Model -> Request -> Page.With Model Msg
page shared _ =
    Page.protected.element
        (\session ->
            { init = init session
            , update = update shared.storage
            , view = view shared
            , subscriptions = \_ -> Sub.none
            }
        )


type alias Model =
    { session : Auth.User
    , state : State
    , toCreate : Maybe NewTransaction
    }


type State
    = Loading
    | Loaded UserResponse
    | Errored String


init : Auth.User -> ( Model, Cmd Msg )
init session =
    loadUser session


type Msg
    = UserLoaded (Result Http.Error UserResponse)
    | ClickedNew
    | Edit NewTransaction
    | ClickedCancel
    | ClickedSubmit (Result (List ( ValidationField, String )) (Valid NewTransaction))
    | Created (Result Http.Error Uuid)


update : Storage -> Msg -> Model -> ( Model, Cmd Msg )
update storage msg model =
    case msg of
        UserLoaded (Ok response) ->
            ( { model | state = Loaded response }, Cmd.none )

        UserLoaded (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        ClickedNew ->
            ( { model | toCreate = Just defaultNew }, Cmd.none )

        Edit new ->
            ( { model | toCreate = Just new }, Cmd.none )

        ClickedCancel ->
            ( { model | toCreate = Nothing }, Cmd.none )

        ClickedSubmit (Ok input) ->
            let
                valid : NewTransaction
                valid =
                    fromValid input
            in
            case String.toFloat valid.amount of
                Just amount ->
                    createTransaction model { amount = -amount, comment = valid.comment }

                _ ->
                    ( model, Cmd.none )

        ClickedSubmit (Err list) ->
            case model.toCreate of
                Just some ->
                    ( { model | toCreate = Just { some | errors = list } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        Created (Ok _) ->
            loadUser model.session

        Created (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )


view : Shared.Model -> Model -> View Msg
view shared model =
    { title = home shared.translations
    , body = Layout.layout Route.Home_ shared (viewUser shared model)
    }


viewUser : Shared.Model -> Model -> List (Element Msg)
viewUser shared model =
    case model.state of
        Loading ->
            [ text (loading shared.translations) ]

        Loaded user ->
            case model.session.role of
                RoleAdmin ->
                    [ text (hello shared.translations user.name)
                    ]

                RoleUser ->
                    case model.toCreate of
                        Just some ->
                            [ viewForm shared some ]

                        Nothing ->
                            [ text (hello shared.translations user.name)
                            , text (balance shared.translations)
                            , el [ Font.size 32, Font.bold ] (text (Currency.toString user.balance))
                            , defaultButton (withdraw shared.translations) ClickedNew
                            ]

        Errored reason ->
            [ text (onError shared.translations reason) ]


viewForm : Shared.Model -> NewTransaction -> Element Msg
viewForm shared toCreate =
    let
        submit : Msg
        submit =
            ClickedSubmit (validate (withdrawValidator shared.translations) toCreate)
    in
    withdrawForm shared.translations toCreate Edit ClickedCancel submit


createTransaction : Model -> Withdraw -> ( Model, Cmd Msg )
createTransaction model withdraw =
    ( { model | state = Loading, toCreate = Nothing }, Api.send Created (createUserTransactions model.session.id withdraw model.session.token) )


loadUser : Auth.User -> ( Model, Cmd Msg )
loadUser session =
    ( { session = session, state = Loading, toCreate = Nothing }, Api.send UserLoaded (readUsers session.id session.token) )
