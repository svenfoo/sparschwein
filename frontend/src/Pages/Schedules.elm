module Pages.Schedules exposing (Model, Msg, State, page)

import Api
import Api.Data exposing (ScheduleResponse, UpdateSchedule, UserResponse)
import Api.Request.Default exposing (deleteSchedules, readAllSchedules, updateSchedules)
import Auth
import Currency
import Element exposing (Element, text)
import Forms.ScheduleForm exposing (EditSchedule, editForm, editScheduleValidator, stringFromFrequency)
import Forms.Validators exposing (ValidationField)
import Gen.Route as Route
import Http
import Page
import Problem exposing (isUnauthenticated)
import Request exposing (Request)
import Shared
import Storage exposing (Storage)
import Translations.Buttons exposing (delete, edit, newSchedule)
import Translations.Forms
import Translations.Labels exposing (loading, onError)
import Translations.Titles exposing (schedules)
import UI.Button exposing (defaultButton)
import UI.Card exposing (keyedCard)
import UI.Dialog exposing (defaultDialog)
import UI.Layout as Layout
import Uuid exposing (Uuid)
import Validate exposing (Valid, fromValid, validate)
import View exposing (View)


page : Shared.Model -> Request -> Page.With Model Msg
page shared req =
    Page.protected.element
        (\session ->
            { init = init session
            , update = update req shared.storage
            , view = view shared
            , subscriptions = \_ -> Sub.none
            }
        )


type alias Model =
    { session : Auth.User
    , state : State
    , toUpdate : Maybe EditSchedule
    , toDelete : Maybe Uuid
    }


type State
    = Loading
    | Loaded ( List ScheduleResponse, List UserResponse )
    | Errored String


init : Auth.User -> ( Model, Cmd Msg )
init session =
    loadSchedules session


type Msg
    = SchedulesLoaded (Result Http.Error (List ScheduleResponse))
    | ScheduleDeleted (Result Http.Error ())
    | UpdateScheduled (Result Http.Error ())
    | ClickedNew
    | Edit EditSchedule
    | ClickedCancelEdit
    | ClickedSubmitEdit (Result (List ( ValidationField, String )) (Valid EditSchedule))
    | ClickedDelete Uuid
    | ClickedCancelDelete
    | ClickedSubmitDelete


update : Request -> Storage -> Msg -> Model -> ( Model, Cmd Msg )
update req storage msg model =
    case msg of
        SchedulesLoaded (Ok list) ->
            ( { model | state = Loaded ( list, [] ) }, Cmd.none )

        SchedulesLoaded (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        ScheduleDeleted (Ok _) ->
            loadSchedules model.session

        ScheduleDeleted (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        UpdateScheduled (Ok _) ->
            loadSchedules model.session

        UpdateScheduled (Err err) ->
            if isUnauthenticated err then
                ( model, Storage.signOut storage )

            else
                ( { model | state = Errored (Problem.toString err) }, Cmd.none )

        ClickedNew ->
            ( model, Request.pushRoute Route.Schedules__New req )

        Edit schedule ->
            ( { model | toUpdate = Just schedule }, Cmd.none )

        ClickedCancelEdit ->
            ( { model | toUpdate = Nothing }, Cmd.none )

        ClickedSubmitEdit (Ok input) ->
            let
                valid : EditSchedule
                valid =
                    fromValid input
            in
            case String.toFloat valid.amount of
                Just amount ->
                    updateSchedule model.session valid.id { frequency = valid.frequency, amount = amount }

                _ ->
                    ( model, Cmd.none )

        ClickedSubmitEdit (Err list) ->
            case model.toUpdate of
                Just some ->
                    ( { model | toUpdate = Just { some | errors = list } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        ClickedDelete uuid ->
            ( { model | toDelete = Just uuid }, Cmd.none )

        ClickedCancelDelete ->
            ( { model | toDelete = Nothing }, Cmd.none )

        ClickedSubmitDelete ->
            case model.toDelete of
                Just uuid ->
                    deleteSchedule model.session uuid

                Nothing ->
                    ( model, Cmd.none )


view : Shared.Model -> Model -> View Msg
view shared model =
    { title = schedules shared.translations
    , body =
        Layout.layout Route.Schedules shared (viewSchedules shared model)
    }


viewSchedules : Shared.Model -> Model -> List (Element Msg)
viewSchedules shared model =
    case model.state of
        Loading ->
            [ text (loading shared.translations) ]

        Loaded ( schedules, _ ) ->
            case ( model.toUpdate, model.toDelete ) of
                ( Just toUpdate, _ ) ->
                    [ editSchedule shared toUpdate ]

                ( _, Just _ ) ->
                    [ defaultDialog shared.translations ClickedCancelDelete ClickedSubmitDelete ]

                _ ->
                    defaultButton (newSchedule shared.translations) ClickedNew :: List.map (viewSchedule shared) schedules

        Errored reason ->
            [ text (onError shared.translations reason) ]


viewSchedule : Shared.Model -> ScheduleResponse -> Element Msg
viewSchedule shared schedule =
    let
        body : List (Element Msg)
        body =
            [ text (stringFromFrequency shared.translations schedule.frequency)
            , text (Currency.toString schedule.amount)
            ]

        buttons : List (Element Msg)
        buttons =
            [ defaultButton (edit shared.translations) (Edit (editScheduleFromSchedule shared schedule))
            , defaultButton (delete shared.translations) (ClickedDelete schedule.id)
            ]
    in
    keyedCard { title = schedule.name, rightLabel = "", body = body, onClick = Nothing, buttons = buttons } schedule.id


editSchedule : Shared.Model -> EditSchedule -> Element Msg
editSchedule shared schedule =
    let
        submit : Msg
        submit =
            ClickedSubmitEdit (validate (editScheduleValidator shared.translations) schedule)
    in
    editForm shared.translations schedule Edit ClickedCancelEdit submit


deleteSchedule : Auth.User -> Uuid -> ( Model, Cmd Msg )
deleteSchedule session uuid =
    ( { session = session, state = Loading, toUpdate = Nothing, toDelete = Nothing }
    , Api.send ScheduleDeleted (deleteSchedules uuid session.token)
    )


updateSchedule : Auth.User -> Uuid -> UpdateSchedule -> ( Model, Cmd Msg )
updateSchedule session id schedule =
    ( { session = session, state = Loading, toUpdate = Nothing, toDelete = Nothing }
    , Api.send UpdateScheduled (updateSchedules id schedule session.token)
    )


loadSchedules : Auth.User -> ( Model, Cmd Msg )
loadSchedules session =
    ( { session = session, state = Loading, toUpdate = Nothing, toDelete = Nothing }
    , Api.send SchedulesLoaded (readAllSchedules session.token)
    )


editScheduleFromSchedule : Shared.Model -> ScheduleResponse -> EditSchedule
editScheduleFromSchedule shared model =
    { id = model.id
    , title = Translations.Forms.editSchedule shared.translations model.name
    , frequency = model.frequency
    , amount = String.fromFloat model.amount
    , errors = []
    }
