module CurrencyTests exposing (suite)

import Currency
import Expect
import Fuzz exposing (float)
import String
import Test exposing (Test, describe, fuzz, test)


suite : Test
suite =
    describe "The Currency module"
        [ describe "Currency.toString"
            [ test "can format 0"
                (\_ ->
                    let
                        input : Float
                        input =
                            0.000001

                        output : String
                        output =
                            "0,00€"
                    in
                    Expect.equal (Currency.toString input) output
                )
            , fuzz float
                "contains a comma"
                (\input ->
                    let
                        output : String
                        output =
                            Currency.toString input
                    in
                    Expect.true output (String.contains "," output)
                )
            , fuzz float
                "contains a currency sign"
                (\input ->
                    let
                        output : String
                        output =
                            Currency.toString input
                    in
                    Expect.true output (String.contains "€" output)
                )
            ]
        ]
