CREATE TABLE raw_transactions (
  id UUID PRIMARY KEY,
  creator_id UUID REFERENCES users (id),
  receiver_id UUID NOT NULL REFERENCES users (id),
  amount FLOAT NOT NULL,
  comment TEXT NOT NULL DEFAULT '',
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE INDEX transaction_time_idx ON raw_transactions (created_at);
SELECT diesel_manage_updated_at('raw_transactions');

CREATE FUNCTION update_balance() RETURNS trigger AS $update_balance$
BEGIN
    IF (TG_OP = 'INSERT') THEN
        UPDATE users SET balance = balance + NEW.amount WHERE id = NEW.receiver_id;
    END IF;
    RETURN NULL;
END;
$update_balance$ LANGUAGE plpgsql;

CREATE TRIGGER update_balance AFTER INSERT ON raw_transactions
    FOR EACH ROW EXECUTE FUNCTION update_balance();

CREATE VIEW transactions AS
SELECT raw_transactions.id,
       raw_transactions.creator_id,
       creators.name AS creator_name,
       raw_transactions.receiver_id,
       receivers.name AS receiver_name,
       raw_transactions.amount,
       raw_transactions.comment,
       raw_transactions.created_at,
       raw_transactions.updated_at
FROM raw_transactions
         LEFT OUTER JOIN users as creators ON (raw_transactions.creator_id = creators.id)
         LEFT OUTER JOIN users as receivers ON (raw_transactions.receiver_id = receivers.id);
