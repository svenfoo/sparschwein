DROP VIEW IF EXISTS transactions;

DROP INDEX IF EXISTS transaction_time_idx;

DROP TRIGGER update_balance ON raw_transactions;

DROP FUNCTION IF EXISTS update_balance();

DROP table raw_transactions;
