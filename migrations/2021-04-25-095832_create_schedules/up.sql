CREATE TYPE frequency_enum AS ENUM ('monthly', 'weekly');

CREATE TABLE schedules (
  id UUID PRIMARY KEY,
  user_id UUID NOT NULL REFERENCES users (id),
  frequency frequency_enum NOT NULL,
  amount FLOAT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE INDEX schedule_amount_idx ON schedules (amount);
SELECT diesel_manage_updated_at('schedules');
